/**
 * RemoteConnectionImpl - Klasa zawiera rzeczywisty JDBC-ODBC Connection. 
 * Pełni rolę zdalnej nakładki nad JDBC-ODBC Connection.
 */

package rso.driver.remote;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

public class RemoteConnectionImpl extends UnicastRemoteObject
        implements IRemoteConnection {

    //The actual JDBC-ODBC connection
    private Connection sqlConnection;
    private List<Connection> remoteConnections;

    /**
     * Constructor to create RemoteConnectionImpl with JDBC-ODBC connection
     */
    public RemoteConnectionImpl(Connection con, List<Connection> sqlCon) throws RemoteException {
        super();
        sqlConnection = con;
        if (sqlCon != null) {
            remoteConnections = sqlCon;
        } else {
            remoteConnections = new ArrayList<>();
        }
    }

    @Override
    public RemoteStatementImpl createStatement() throws RemoteException {
        RemoteStatementImpl statement = null;
        try {
            List<Statement> statements = new ArrayList<>(remoteConnections.size());
            for (Connection connection : remoteConnections) {
                statements.add(connection.createStatement());
            }
            statement = new RemoteStatementImpl(sqlConnection.createStatement(), statements);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
        return statement;

    }


    public PreparedStatement prepareStatement(String sql) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public CallableStatement prepareCall(String sql) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public String nativeSQL(String sql) throws RemoteException {
        try {
            String result = sqlConnection.nativeSQL(sql);
            for (Connection remoteConnection : remoteConnections) {
                remoteConnection.nativeSQL(sql);
            }
            return result;
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setAutoCommit(boolean autoCommit) throws RemoteException {
        try {
            sqlConnection.setAutoCommit(autoCommit);
            for (Connection remoteConnection : remoteConnections) {
                remoteConnection.setAutoCommit(autoCommit);
            }
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public boolean getAutoCommit() throws RemoteException {
        try {
            return sqlConnection.getAutoCommit();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void commit() throws RemoteException {
        try {
            sqlConnection.commit();
            for (Connection remoteConnection : remoteConnections) {
                remoteConnection.commit();
            }
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void rollback() throws RemoteException {
        try {
            sqlConnection.rollback();
            for (Connection remoteConnection : remoteConnections) {
                remoteConnection.rollback();
            }
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void close() throws RemoteException {
        try {
            sqlConnection.close();
            for (Connection remoteConnection : remoteConnections) {
                remoteConnection.close();
            }
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public boolean isClosed() throws RemoteException {
        try {
            boolean isClosed = sqlConnection.isClosed();
            for (Connection remoteConnection : remoteConnections) {
                isClosed = remoteConnection.isClosed();
            }
            return isClosed;
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public DatabaseMetaData getMetaData() throws RemoteException {
        try {
            return sqlConnection.getMetaData();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setReadOnly(boolean readOnly) throws RemoteException {
        try {
            sqlConnection.setReadOnly(readOnly);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public boolean isReadOnly() throws RemoteException {
        try {
            return sqlConnection.isReadOnly();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setCatalog(String catalog) throws RemoteException {
        try {
            sqlConnection.setCatalog(catalog);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public String getCatalog() throws RemoteException {
        try {
            return sqlConnection.getCatalog();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setTransactionIsolation(int level) throws RemoteException {
        try {
            sqlConnection.setTransactionIsolation(level);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public int getTransactionIsolation() throws RemoteException {
        try {
            return sqlConnection.getTransactionIsolation();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public SQLWarning getWarnings() throws RemoteException {
        try {
            return sqlConnection.getWarnings();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void clearWarnings() throws RemoteException {
        try {
            sqlConnection.clearWarnings();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public RemoteStatementImpl createStatement(int resultSetType, int resultSetConcurrency) throws RemoteException {
        try {
            Statement localStatement = sqlConnection.createStatement(resultSetType, resultSetConcurrency);
            List<Statement> remoteStatements = new ArrayList<>(remoteConnections.size());
            for (Connection connection : remoteConnections) {
                remoteStatements.add(connection.createStatement(resultSetType, resultSetConcurrency));
            }
            return new RemoteStatementImpl(localStatement, remoteStatements);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public Map<String, Class<?>> getTypeMap() throws RemoteException {
        try {
            return sqlConnection.getTypeMap();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setTypeMap(Map<String, Class<?>> map) throws RemoteException {
        try {
            sqlConnection.setTypeMap(map);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setHoldability(int holdability) throws RemoteException {
        try {
            sqlConnection.setHoldability(holdability);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public int getHoldability() throws RemoteException {
        try {
            return sqlConnection.getHoldability();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public Savepoint setSavepoint() throws RemoteException {
        try {
            return sqlConnection.setSavepoint();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public Savepoint setSavepoint(String name) throws RemoteException {
        try {
            return sqlConnection.setSavepoint(name);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void rollback(Savepoint savepoint) throws RemoteException {
        try {
            sqlConnection.rollback(savepoint);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void releaseSavepoint(Savepoint savepoint) throws RemoteException {
        try {
            sqlConnection.releaseSavepoint(savepoint);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public RemoteStatementImpl createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws RemoteException {
        try {
            Statement localStatement = sqlConnection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
            List<Statement> remoteStatements = new ArrayList<>(remoteConnections.size());

            return new RemoteStatementImpl(localStatement, remoteStatements);

        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws RemoteException {
        throw new RemoteException("not supported");
    }


    public Clob createClob() throws RemoteException {
        try {
            return sqlConnection.createClob();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public Blob createBlob() throws RemoteException {
        try {
            return sqlConnection.createBlob();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public NClob createNClob() throws RemoteException {
        try {
            return sqlConnection.createNClob();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public SQLXML createSQLXML() throws RemoteException {
        try {
            return sqlConnection.createSQLXML();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public boolean isValid(int timeout) throws RemoteException {
        try {
            return sqlConnection.isValid(timeout);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setClientInfo(String name, String value) throws RemoteException {
        try {
            sqlConnection.setClientInfo(name, value);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setClientInfo(Properties properties) throws RemoteException {
        try {
            sqlConnection.setClientInfo(properties);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public String getClientInfo(String name) throws RemoteException {
        try {
            return sqlConnection.getClientInfo(name);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public Properties getClientInfo() throws RemoteException {
        try {
            return sqlConnection.getClientInfo();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public Array createArrayOf(String typeName, Object[] elements) throws RemoteException {
        try {
            return sqlConnection.createArrayOf(typeName, elements);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public Struct createStruct(String typeName, Object[] attributes) throws RemoteException {
        try {
            return sqlConnection.createStruct(typeName, attributes);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setSchema(String schema) throws RemoteException {
        try {
            sqlConnection.setSchema(schema);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public String getSchema() throws RemoteException {
        try {
            return sqlConnection.getSchema();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void abort(Executor executor) throws RemoteException {
        try {
            sqlConnection.abort(executor);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public void setNetworkTimeout(Executor executor, int milliseconds) throws RemoteException {
        try {
            sqlConnection.setNetworkTimeout(executor, milliseconds);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public int getNetworkTimeout() throws RemoteException {
        try {
            return sqlConnection.getNetworkTimeout();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public <T> T unwrap(Class<T> iface) throws RemoteException {
        try {
            return sqlConnection.unwrap(iface);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }


    public boolean isWrapperFor(Class<?> iface) throws RemoteException {
        try {
            return sqlConnection.isWrapperFor(iface);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }
}
