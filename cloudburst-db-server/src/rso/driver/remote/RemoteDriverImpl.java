/**
 * RemoteConnectionImpl - Klasa pełni rolę głównego serwera RMI. Driver klienta
 * łączy się z obiektami tej klasy przez RMI i ustanawia Connection. 
 */

package rso.driver.remote;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class RemoteDriverImpl extends UnicastRemoteObject
        implements IRemoteDriver {

    private static List<String> remoteUrls = new LinkedList<>();

    public RemoteDriverImpl() throws RemoteException {
        super();
    }

    /**
     * This is the main starting method of RMI server. It expects
     * Data Source Name,  Data Source user, Data source password as
     * first three arguments.
     */
    public static void main(String args[]) {

        try {
            java.rmi.registry.LocateRegistry.createRegistry(1099);
            System.out.println("RMI registry ready.");
        } catch (RemoteException e) {
            System.out.println("Exception starting RMI registry:");
            e.printStackTrace();
        }
        System.setProperty("java.rmi.server.logCalls", "true");

        System.setSecurityManager(new SecurityManager());

        try {
            //get the Data Source Name,  Data Source user, Data source password and log level
//            ResourceBundle settingsBundle = ResourceBundle.getBundle("DriverSettings");
//            DSN = settingsBundle.getString("DSN");
//            dsUser = settingsBundle.getString("User");
//            dsPassword = settingsBundle.getString("Password");

            //Creaete an object of RemoteDriverImpl to register with RMI naming service
            //After this the Type-III client driver layer can access this object.
            RemoteDriverImpl serverInstance = new RemoteDriverImpl();
            Naming.rebind("//"+args[0]+"/"+"RemoteDriver", serverInstance);


            if (args.length >= 2) {
                String name = "rmi://" + args[1] + ":1099" + "/RemoteDriver";
                System.out.println("name: "+name);
                IRemoteDriver remoteDriver = (IRemoteDriver) Naming.lookup(name);
                InetAddress addr = InetAddress.getLocalHost();
                String s = addr.getHostAddress();
                remoteDriver.registerNode(s);
            }

            //Wait for close server command
            System.out.println();
            System.out.println("Remote Driver server has started successfully...");
            System.out.println();
            System.out.println("Press 'Y' to stop the server...");

            while (System.in.read() != 'Y') {
            }

            System.out.println("Closing the remote server");
            System.exit(0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method creates a JDBC-ODBC connection and then returns a reference
     * to the remote interface of the RemoteConnectionImpl object holding
     * JDBC-ODBC connection.
     */
    public IRemoteConnection getConnection(String user, String password, String dbName, boolean fromClient) throws RemoteException, SQLException {
        if (dbName == null) {
            dbName = "test";
        }
        if (user == null)  {
            user = "sa";
        }
        if (password == null) {
            password = "";
        }
        try {
            Class.forName("rso.driver.JWDriver");
        } catch (ClassNotFoundException e) {
            throw new RemoteException(e.getMessage());
        }
        String URL = "jdbc:h2:"+dbName;
        Connection sqlCon = DriverManager.getConnection(URL, user, password);
        List<Connection> remoteConnections = null;
        Properties loginProperties = new Properties();
        loginProperties.setProperty("user", user);
        loginProperties.setProperty("password", password);
        loginProperties.setProperty("database", dbName);

        if (fromClient) {
            remoteConnections = new ArrayList<>(remoteUrls.size());
            for (String url : remoteUrls) {
                remoteConnections.add(DriverManager.getConnection("xjdbc:JWDriver:"+url, loginProperties));
            }
        }
        RemoteConnectionImpl connectionInstance = new RemoteConnectionImpl(sqlCon, remoteConnections);
        return connectionInstance;
    }

    public void registerNode(String url) {
        System.out.println("Registering node "+url);
        remoteUrls.add(url);
    }
}