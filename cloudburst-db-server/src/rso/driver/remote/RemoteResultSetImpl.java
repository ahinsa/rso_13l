/**
 * RemoteResultSetImpl - Klasa zawiera rzeczywisty JDBC-ODBC ResultSet. 
 * Pełni rolę zdalnej nakładki nad JDBC-ODBC ResultSet.
 */

package rso.driver.remote;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class RemoteResultSetImpl extends UnicastRemoteObject
        implements IRemoteResultSet {
    //JDBC-ODBC ResultSet
    private Queue<ResultSet> resultSets;

    /**
     * Constructor for creating RemoteResultSetImpl with JDBC ResultSet
     */
    public RemoteResultSetImpl(ResultSet rs, List<ResultSet> remoteResultSets) throws RemoteException {
        super();
        if (remoteResultSets != null) {
            resultSets = new ArrayDeque<>(remoteResultSets);
        } else {
            resultSets = new ArrayDeque<>();
        }
        resultSets.add(rs);
    }

    @Override
    public boolean next() throws RemoteException {
        try {
            ResultSet rs = getNextResultSet();
            if (rs == null) {
                return false;
            }
            boolean result = rs.next();
            if (result == false) {
                resultSets.poll();
                if (resultSets.peek() != null) {
                    return getNextResultSet().next();
                } else {
                    return false;
                }
            }
            return true;
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void close() throws RemoteException {
        try {
            getNextResultSet().close();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean wasNull() throws RemoteException {
        try {
            return getNextResultSet().wasNull();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public String getString(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getString(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }

    }

    @Override
    public boolean getBoolean(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getBoolean(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public byte getByte(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getByte(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public short getShort(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getShort(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getInt(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getInt(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public long getLong(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getLong(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public float getFloat(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getFloat(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public double getDouble(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getDouble(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex, int scale) throws RemoteException {
        try {
            return getNextResultSet().getBigDecimal(columnIndex, scale);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public byte[] getBytes(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getBytes(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Date getDate(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getDate(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Time getTime(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getTime(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getTimestamp(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public InputStream getAsciiStream(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getAsciiStream(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public InputStream getUnicodeStream(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getUnicodeStream(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public InputStream getBinaryStream(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getBinaryStream(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public String getString(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getString(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean getBoolean(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getBoolean(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public byte getByte(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getByte(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public short getShort(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getShort(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getInt(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getInt(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public long getLong(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getLong(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public float getFloat(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getFloat(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public double getDouble(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getDouble(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel, int scale) throws RemoteException {
        return getBigDecimal(columnLabel, scale);
    }

    @Override
    public byte[] getBytes(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getBytes(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Date getDate(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getDate(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Time getTime(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getTime(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getTimestamp(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public InputStream getAsciiStream(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getAsciiStream(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public InputStream getUnicodeStream(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getUnicodeStream(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public InputStream getBinaryStream(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getBinaryStream(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public SQLWarning getWarnings() throws RemoteException {
        try {
            return getNextResultSet().getWarnings();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void clearWarnings() throws RemoteException {
        try {
            getNextResultSet().clearWarnings();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public String getCursorName() throws RemoteException {
        try {
            return getNextResultSet().getCursorName();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public ResultSetMetaData getMetaData() throws RemoteException {
        try {
            return getNextResultSet().getMetaData();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Object getObject(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getObject(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Object getObject(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getObject(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int findColumn(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().findColumn(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Reader getCharacterStream(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getCharacterStream(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Reader getCharacterStream(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getCharacterStream(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getBigDecimal(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getBigDecimal(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean isBeforeFirst() throws RemoteException {
        try {
            return getNextResultSet().isBeforeFirst();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean isAfterLast() throws RemoteException {
        try {
            return getNextResultSet().isAfterLast();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean isFirst() throws RemoteException {
        try {
            return getNextResultSet().isFirst();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean isLast() throws RemoteException {
        try {
            return getNextResultSet().isLast();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void beforeFirst() throws RemoteException {
        try {
            getNextResultSet().beforeFirst();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void afterLast() throws RemoteException {
        try {
            getNextResultSet().afterLast();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean first() throws RemoteException {
        try {
            return getNextResultSet().first();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean last() throws RemoteException {
        try {
            return getNextResultSet().last();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getRow() throws RemoteException {
        try {
            return getNextResultSet().getRow();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean absolute(int row) throws RemoteException {
        try {
            return getNextResultSet().absolute(row);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean relative(int rows) throws RemoteException {
        try {
            return getNextResultSet().relative(rows);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean previous() throws RemoteException {
        try {
            return getNextResultSet().previous();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void setFetchDirection(int direction) throws RemoteException {
        try {
            getNextResultSet().setFetchDirection(direction);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getFetchDirection() throws RemoteException {
        try {
            return getNextResultSet().getFetchDirection();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void setFetchSize(int rows) throws RemoteException {
        try {
            getNextResultSet().setFetchSize(rows);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getFetchSize() throws RemoteException {
        try {
            return getNextResultSet().getFetchSize();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getType() throws RemoteException {
        try {
            return getNextResultSet().getType();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getConcurrency() throws RemoteException {
        try {
            return getNextResultSet().getConcurrency();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean rowUpdated() throws RemoteException {
        try {
            return getNextResultSet().rowUpdated();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean rowInserted() throws RemoteException {
        try {
            return getNextResultSet().rowInserted();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean rowDeleted() throws RemoteException {
        try {
            return getNextResultSet().rowDeleted();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNull(int columnIndex) throws RemoteException {
        try {
            getNextResultSet().updateNull(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBoolean(int columnIndex, boolean x) throws RemoteException {
        try {
            getNextResultSet().updateBoolean(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateByte(int columnIndex, byte x) throws RemoteException {
        try {
            getNextResultSet().updateByte(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateShort(int columnIndex, short x) throws RemoteException {
        try {
            getNextResultSet().updateShort(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateInt(int columnIndex, int x) throws RemoteException {
        try {
            getNextResultSet().updateInt(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateLong(int columnIndex, long x) throws RemoteException {
        try {
            getNextResultSet().updateLong(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateFloat(int columnIndex, float x) throws RemoteException {
        try {
            getNextResultSet().updateFloat(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateDouble(int columnIndex, double x) throws RemoteException {
        try {
            getNextResultSet().updateDouble(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBigDecimal(int columnIndex, BigDecimal x) throws RemoteException {
        try {
            getNextResultSet().updateBigDecimal(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateString(int columnIndex, String x) throws RemoteException {
        try {
            getNextResultSet().updateString(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBytes(int columnIndex, byte[] x) throws RemoteException {
        try {
            getNextResultSet().updateBytes(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateDate(int columnIndex, Date x) throws RemoteException {
        try {
            getNextResultSet().updateDate(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateTime(int columnIndex, Time x) throws RemoteException {
        try {
            getNextResultSet().updateTime(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateTimestamp(int columnIndex, Timestamp x) throws RemoteException {
        try {
            getNextResultSet().updateTimestamp(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, int length) throws RemoteException {
        try {
            getNextResultSet().updateAsciiStream(columnIndex,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, int length) throws RemoteException {
        try {
            getNextResultSet().updateBinaryStream(columnIndex,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, int length) throws RemoteException {
        try {
            getNextResultSet().updateCharacterStream(columnIndex,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateObject(int columnIndex, Object x, int scaleOrLength) throws RemoteException {
        try {
            getNextResultSet().updateObject(columnIndex,x,scaleOrLength);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateObject(int columnIndex, Object x) throws RemoteException {
        try {
            getNextResultSet().updateObject(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNull(String columnLabel) throws RemoteException {
        try {
            getNextResultSet().updateNull(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBoolean(String columnLabel, boolean x) throws RemoteException {
        try {
            getNextResultSet().updateBoolean(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateByte(String columnLabel, byte x) throws RemoteException {
        try {
            getNextResultSet().updateByte(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateShort(String columnLabel, short x) throws RemoteException {
        try {
            getNextResultSet().updateShort(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateInt(String columnLabel, int x) throws RemoteException {
        try {
            getNextResultSet().updateInt(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateLong(String columnLabel, long x) throws RemoteException {
        try {
            getNextResultSet().updateLong(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateFloat(String columnLabel, float x) throws RemoteException {
        try {
            getNextResultSet().updateFloat(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateDouble(String columnLabel, double x) throws RemoteException {
        try {
            getNextResultSet().updateDouble(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBigDecimal(String columnLabel, BigDecimal x) throws RemoteException {
        try {
            getNextResultSet().updateBigDecimal(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateString(String columnLabel, String x) throws RemoteException {
        try {
            getNextResultSet().updateString(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBytes(String columnLabel, byte[] x) throws RemoteException {
        try {
            getNextResultSet().updateBytes(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateDate(String columnLabel, Date x) throws RemoteException {
        try {
            getNextResultSet().updateDate(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateTime(String columnLabel, Time x) throws RemoteException {
        try {
            getNextResultSet().updateTime(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateTimestamp(String columnLabel, Timestamp x) throws RemoteException {
        try {
            getNextResultSet().updateTimestamp(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, int length) throws RemoteException {
        try {
            getNextResultSet().updateAsciiStream(columnLabel,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, int length) throws RemoteException {
        try {
            getNextResultSet().updateBinaryStream(columnLabel,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, int length) throws RemoteException {
        try {
            getNextResultSet().updateCharacterStream(columnLabel,reader,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateObject(String columnLabel, Object x, int scaleOrLength) throws RemoteException {
        try {
            getNextResultSet().updateObject(columnLabel,x,scaleOrLength);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateObject(String columnLabel, Object x) throws RemoteException {
        try {
            getNextResultSet().updateObject(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void insertRow() throws RemoteException {
        try {
            getNextResultSet().insertRow();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateRow() throws RemoteException {
        try {
            getNextResultSet().updateRow();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void deleteRow() throws RemoteException {
        try {
            getNextResultSet().insertRow();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void refreshRow() throws RemoteException {
        try {
            getNextResultSet().refreshRow();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void cancelRowUpdates() throws RemoteException {
        try {
            getNextResultSet().cancelRowUpdates();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void moveToInsertRow() throws RemoteException {
        try {
            getNextResultSet().moveToInsertRow();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void moveToCurrentRow() throws RemoteException {
        try {
            getNextResultSet().moveToCurrentRow();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Statement getStatement() throws RemoteException {
        try {
            return getNextResultSet().getStatement();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Object getObject(int columnIndex, Map<String, Class<?>> map) throws RemoteException {
        try {
            return getNextResultSet().getObject(columnIndex,map);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Ref getRef(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getRef(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Blob getBlob(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getBlob(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Clob getClob(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getClob(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Array getArray(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getArray(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Object getObject(String columnLabel, Map<String, Class<?>> map) throws RemoteException {
        try {
            return getNextResultSet().getObject(columnLabel,map);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Ref getRef(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getRef(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Blob getBlob(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getBlob(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Clob getClob(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getClob(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Array getArray(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getArray(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Date getDate(int columnIndex, Calendar cal) throws RemoteException {
        try {
            return getNextResultSet().getDate(columnIndex,cal);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Date getDate(String columnLabel, Calendar cal) throws RemoteException {
        try {
            return getNextResultSet().getDate(columnLabel,cal);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Time getTime(int columnIndex, Calendar cal) throws RemoteException {
        try {
            return getNextResultSet().getTime(columnIndex,cal);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Time getTime(String columnLabel, Calendar cal) throws RemoteException {
        try {
            return getNextResultSet().getTime(columnLabel,cal);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(int columnIndex, Calendar cal) throws RemoteException {
        try {
            return getNextResultSet().getTimestamp(columnIndex,cal);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(String columnLabel, Calendar cal) throws RemoteException {
        try {
            return getNextResultSet().getTimestamp(columnLabel,cal);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public URL getURL(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getURL(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public URL getURL(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getURL(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateRef(int columnIndex, Ref x) throws RemoteException {
        try {
            getNextResultSet().updateRef(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateRef(String columnLabel, Ref x) throws RemoteException {
        try {
            getNextResultSet().updateRef(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(int columnIndex, Blob x) throws RemoteException {
        try {
            getNextResultSet().updateBlob(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(String columnLabel, Blob x) throws RemoteException {
        try {
            getNextResultSet().updateBlob(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateClob(int columnIndex, Clob x) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateClob(String columnLabel, Clob x) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateArray(int columnIndex, Array x) throws RemoteException {
        try {
            getNextResultSet().updateArray(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateArray(String columnLabel, Array x) throws RemoteException {
        try {
            getNextResultSet().updateArray(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public RowId getRowId(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getRowId(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public RowId getRowId(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getRowId(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateRowId(int columnIndex, RowId x) throws RemoteException {
        try {
            getNextResultSet().updateRowId(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateRowId(String columnLabel, RowId x) throws RemoteException {
        try {
            getNextResultSet().updateRowId(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public int getHoldability() throws RemoteException {
        try {
            return getNextResultSet().getHoldability();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean isClosed() throws RemoteException {
        try {
            return getNextResultSet().isClosed();
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNString(int columnIndex, String nString) throws RemoteException {
        try {
            getNextResultSet().updateNString(columnIndex,nString);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNString(String columnLabel, String nString) throws RemoteException {
        try {
            getNextResultSet().updateNString(columnLabel,nString);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(int columnIndex, NClob nClob) throws RemoteException {
        try {
            getNextResultSet().updateNClob(columnIndex,nClob);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(String columnLabel, NClob nClob) throws RemoteException {
        try {
            getNextResultSet().updateNClob(columnLabel,nClob);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public NClob getNClob(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getNClob(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public NClob getNClob(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getNClob(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public SQLXML getSQLXML(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getSQLXML(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public SQLXML getSQLXML(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getSQLXML(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws RemoteException {
        try {
            getNextResultSet().updateSQLXML(columnIndex,xmlObject);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws RemoteException {
        try {
            getNextResultSet().updateSQLXML(columnLabel,xmlObject);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public String getNString(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getNString(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public String getNString(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getNString(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Reader getNCharacterStream(int columnIndex) throws RemoteException {
        try {
            return getNextResultSet().getNCharacterStream(columnIndex);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public Reader getNCharacterStream(String columnLabel) throws RemoteException {
        try {
            return getNextResultSet().getNCharacterStream(columnLabel);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x, long length) throws RemoteException {
        try {
            getNextResultSet().updateNCharacterStream(columnIndex,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws RemoteException {
        try {
            getNextResultSet().updateNCharacterStream(columnLabel,reader,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, long length) throws RemoteException {
        try {
            getNextResultSet().updateAsciiStream(columnIndex,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, long length) throws RemoteException {
        try {
            getNextResultSet().updateBinaryStream(columnIndex,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, long length) throws RemoteException {
        try {
            getNextResultSet().updateCharacterStream(columnIndex,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, long length) throws RemoteException {
        try {
            getNextResultSet().updateAsciiStream(columnLabel,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, long length) throws RemoteException {
        try {
            getNextResultSet().updateBinaryStream(columnLabel,x,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, long length) throws RemoteException {
        try {
            getNextResultSet().updateCharacterStream(columnLabel,reader,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream, long length) throws RemoteException {
        try {
            getNextResultSet().updateBlob(columnIndex,inputStream,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream, long length) throws RemoteException {
        try {
            getNextResultSet().updateBlob(columnLabel,inputStream,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateClob(int columnIndex, Reader reader, long length) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnIndex,reader,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateClob(String columnLabel, Reader reader, long length) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnLabel,reader,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(int columnIndex, Reader reader, long length) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnIndex,reader,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(String columnLabel, Reader reader, long length) throws RemoteException {
        try {
            getNextResultSet().updateNClob(columnLabel,reader,length);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x) throws RemoteException {
        try {
            getNextResultSet().updateNCharacterStream(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader) throws RemoteException {
        try {
            getNextResultSet().updateNCharacterStream(columnLabel,reader);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x) throws RemoteException {
        try {
            getNextResultSet().updateAsciiStream(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x) throws RemoteException {
        try {
            getNextResultSet().updateBinaryStream(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x) throws RemoteException {
        try {
            getNextResultSet().updateCharacterStream(columnIndex,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x) throws RemoteException {
        try {
            getNextResultSet().updateAsciiStream(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x) throws RemoteException {
        try {
            getNextResultSet().updateBinaryStream(columnLabel,x);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnLabel,reader);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream) throws RemoteException {
        try {
            getNextResultSet().updateBlob(columnIndex,inputStream);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream) throws RemoteException {
        try {
            getNextResultSet().updateBlob(columnLabel,inputStream);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateClob(int columnIndex, Reader reader) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnIndex,reader);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateClob(String columnLabel, Reader reader) throws RemoteException {
        try {
            getNextResultSet().updateClob(columnLabel,reader);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(int columnIndex, Reader reader) throws RemoteException {
        try {
            getNextResultSet().updateNClob(columnIndex,reader);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(String columnLabel, Reader reader) throws RemoteException {
        try {
            getNextResultSet().updateNClob(columnLabel,reader);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public <T> T getObject(int columnIndex, Class<T> type) throws RemoteException {
        try {
            return getNextResultSet().getObject(columnIndex,type);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public <T> T getObject(String columnLabel, Class<T> type) throws RemoteException {
        try {
            return getNextResultSet().getObject(columnLabel,type);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws RemoteException {
        try {
            return getNextResultSet().unwrap(iface);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws RemoteException {
        try {
            return getNextResultSet().isWrapperFor(iface);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    private ResultSet getNextResultSet() {
        ResultSet result = resultSets.peek();
        return result;
    }
}