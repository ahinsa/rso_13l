/**
 * RemoteStatementImpl - Klasa zawiera rzeczywisty JDBC-ODBC Statement.
 * Pełni rolę zdalnej nakładki nad JDBC-ODBC Statement.
 */
package rso.driver.remote;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RemoteStatementImpl extends UnicastRemoteObject
        implements IRemoteStatement {

    //JDBC-ODBC statement
    private Statement sqlStatment;
    private List<Statement> remoteStatements;

    /**
     * Constructor for RemoteStatementImpl with JDBC-ODBC Statement
     */
    public RemoteStatementImpl(Statement sqlStmt, List<Statement> statements) throws RemoteException {
        super();
        sqlStatment = sqlStmt;
        if (statements != null) {
            remoteStatements = statements;
        } else {
            remoteStatements = new ArrayList<>();
        }
    }

    @Override
    public RemoteResultSetImpl executeQuery(String sql) throws RemoteException {
        RemoteResultSetImpl resultSet;
        try {
            ResultSet localResultSet= sqlStatment.executeQuery(sql);
            List<ResultSet> remoteResultSets = new ArrayList<>(remoteStatements.size());
            for (Statement statement : remoteStatements) {
                remoteResultSets.add(statement.executeQuery(sql));
            }
            resultSet = new RemoteResultSetImpl(localResultSet, remoteResultSets);
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
        return resultSet;
    }

    @Override
    public int executeUpdate(String sql) throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void close() throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getMaxFieldSize() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setMaxFieldSize(int max) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getMaxRows() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setMaxRows(int max) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setEscapeProcessing(boolean enable) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getQueryTimeout() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setQueryTimeout(int seconds) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void cancel() throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SQLWarning getWarnings() throws RemoteException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void clearWarnings() throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setCursorName(String name) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean execute(String sql) throws RemoteException {
        try {
            if (sql.toUpperCase().trim().startsWith("INSERT INTO")) {
                int index = sql.hashCode()%(remoteStatements.size()+1);
                if (index == 0) {
                    return sqlStatment.execute(sql);
                } else {
                    return remoteStatements.get(Math.abs(index)-1).execute(sql);
                }
            } else {
                boolean result = sqlStatment.execute(sql);
                for (Statement statement : remoteStatements) {
                    result = statement.execute(sql);
                }
                return result;
            }
        } catch (SQLException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    @Override
    public ResultSet getResultSet() throws RemoteException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getUpdateCount() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean getMoreResults() throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setFetchDirection(int direction) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getFetchDirection() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setFetchSize(int rows) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getFetchSize() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getResultSetConcurrency() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getResultSetType() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addBatch(String sql) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void clearBatch() throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int[] executeBatch() throws RemoteException {
        return new int[0];  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Connection getConnection() throws RemoteException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean getMoreResults(int current) throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ResultSet getGeneratedKeys() throws RemoteException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int executeUpdate(String sql, int autoGeneratedKeys) throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int executeUpdate(String sql, int[] columnIndexes) throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int executeUpdate(String sql, String[] columnNames) throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean execute(String sql, int autoGeneratedKeys) throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean execute(String sql, int[] columnIndexes) throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean execute(String sql, String[] columnNames) throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getResultSetHoldability() throws RemoteException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isClosed() throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setPoolable(boolean poolable) throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isPoolable() throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void closeOnCompletion() throws RemoteException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isCloseOnCompletion() throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws RemoteException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws RemoteException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}