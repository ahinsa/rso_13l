package rso.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.LinkedList;
import java.util.List;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;

/**
 * Klasa Manager
 */
public class CloudBurst {

    // kolejka zapytan
    public static final String QUEUE1 = "Simple.Test.Queue1";
    // kolejka PING
    public static final String QUEUE2 = "Simple.Test.Queue2";
    
    public static final String myUri = "tcp://localhost:61616";
    private static List<ManagerLocation> managers = null;


    public static void main(String[] args) throws Exception {

        if (args.length > 0) {
            String url = args[0];
            register(url);
        }

        managers = new LinkedList<>();
        // loopback - na potrzeby testów
        managers.add(new ManagerLocation("tcp://localhost:61616"));

        // połączenie z H2 
//            Class.forName("org.h2.Driver");
//            Connection conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
//            conn.close();

        startBroker(); // JMS Broker

        String sqlStatement1 = "CREATE TABLE";
        String sqlStatement2 = "SELECT id, nazwisko FROM pracownik;";

        Receiver receiver = new Receiver();
        receiver.start();

        HeartBeat heartbeat = new HeartBeat();
        heartbeat.start();


        //   register("tcp://localhost:61616");
        Sender.sendMessage("Q", sqlStatement1, "tcp://localhost:61616", CloudBurst.QUEUE1); // potwierdzenie odebrania komunikatu

        Thread.sleep(4000);

           heartbeat.stopListening();
            receiver.stopListening();

           System.exit(0);
    }

    /**
     * JMS Broker
     */
    private static void startBroker() throws Exception {
        BrokerService broker = new BrokerService();
        broker.setUseJmx(true);
        broker.addConnector(myUri);
        broker.start();
    }

    /**
     * JMS ConnectionFactory
     */
    public static ConnectionFactory getJmsConnectionFactory()
            throws JMSException {
        //    String user = ActiveMQConnection.DEFAULT_USER;
        //    String password = ActiveMQConnection.DEFAULT_PASSWORD;
        String url = ActiveMQConnection.DEFAULT_BROKER_URL;

        return new ActiveMQConnectionFactory(url);
    }

    // rejestruje managera w sieci za posrednictwem managera o podanym uri
    private static void register(String uri) {
        // rejestruje nowego managera w sieci
        Sender.sendMessage("REG", myUri, uri, CloudBurst.QUEUE1);
    }

    public static void addNode(String uri, boolean delegate) {

        // true -> manager jest juz znany 
        boolean exists = false;
        // sprawdz czy istnieje taki...
        for (ManagerLocation manager : managers) {
            if (manager.getUri().equalsIgnoreCase(uri)) {
                exists = true;
            }
        }

        // dodaj nowego managera do listy jesli nie istnieje
        if (exists != true) {
            CloudBurst.getManagers().add(new ManagerLocation(uri));
        }

        // rejestruj nowego managera u wszystkich znanych managerow ???
        if ((exists != true) && (delegate == true)) {
            for (ManagerLocation manager : managers) // odpowiedz zwrotna do rejestrowanego (uri)
            {
                Sender.sendMessage("REG", uri, manager.getUri(), CloudBurst.QUEUE1);
            }
        }
    }

    public static List<ManagerLocation> getManagers() {
        return managers;
    }

    public static String getMyUri() {
        return myUri;
    }

    public static ManagerLocation findManagerByURI(String uri) {
        for (ManagerLocation manager : managers) {
            if (manager.getUri().equalsIgnoreCase(uri)) {
                return manager;
            }
        }
        return null;
    }
}
