package rso.manager;

import java.util.Date;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

/**
 * Klasa odpowiedzialna za urzymanie połączenia. Cyklicznie pinguje wszystkich 
 * znanych managerów, kwalifikator wiadomosci - PING, odpowiedz PING_OK. 
 * W przypadku timeouta (10 sec) następuje usuniecie managera z listy.
 */
public class HeartBeat extends Thread {

    private boolean bRunning = false;
    private boolean bStopped = false;
    MessageProtocol msgProtocol = null;

    public void run() {
        System.out.println("Heartbeat() starting");
        bRunning = true;

        Connection connection = null;
        try {
            ConnectionFactory factory = CloudBurst.getJmsConnectionFactory();
            connection = factory.createConnection();
            connection.start();

            msgProtocol = new MessageProtocol();
            Date now = new Date();

            while (bRunning) {
                for(ManagerLocation manager : CloudBurst.getManagers()){
                    Sender.sendMessage("PING", "", manager.getUri(), CloudBurst.QUEUE1);
                    manager.setLastPingedAt(now.getTime());
                    // obsluga timeout-a, usun managera z listy 
                    if (manager.getLastPingedAt()-manager.getLastConfirmationAt()>= 10000) {
                        CloudBurst.getManagers().remove(manager);
                               System.out.println("[TIMEOUT] manager: " + manager.getUri());
                    }
}   
            }
        } catch (JMSException e) {
            System.out.println("Exception occurred: " + e.toString());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
            }
            System.out.println("Hearbeat ended");
            bStopped = true;
            synchronized (this) {
                notifyAll();
            }
        }
    }

    synchronized public void stopListening() {
        System.out.println("Hearbeat.stopListening()");
        bRunning = false;
        while (!bStopped) {
            System.out.println("Hearbeat.stopListening().Waiting for connections to close...");
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
