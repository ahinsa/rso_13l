package rso.manager;

/**
 * Klasa przechowująca adresy hostów
 */
class ManagerLocation {

    private String uri;
    private String user;
    private String password;
    // czas ostatniego pingu
    private long lastPingedAt;
    // czas osatniej otrzymanej odpowiedz
    private long lastConfirmationAt;
    
      public ManagerLocation(String url) {
        this.uri = url;
        lastConfirmationAt = 0;
        lastPingedAt = 0;
    }

    public long getLastConfirmationAt() {
        return lastConfirmationAt;
    }

    public void setLastConfirmationAt(long lastConfirmationAt) {
        this.lastConfirmationAt = lastConfirmationAt;
    }


    public String getUri() {
        return uri;
    }

    public void setUri(String url) {
        this.uri = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getLastPingedAt() {
        return lastPingedAt;
    }

    public void setLastPingedAt(long lastPingedAt) {
        this.lastPingedAt = lastPingedAt;
        if(this.lastConfirmationAt == 0)
            this.lastConfirmationAt = this.lastPingedAt;
    }
}
