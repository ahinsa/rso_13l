package rso.manager;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

/**
 * Klasa implementująca protokół komunikacyjny pomiędzy Managerami. Przetwarza
 * wiadomość przychodzącą message (QUAL,VAL) - (kwalifikator, wartosc) i tworzy
 * odpowiedz na jej podstawie
 */
public class MessageProtocol {

    public Message handleProtocolMessage(Session session, Message message) {
        try {

            MapMessage response = null;

            // kwalifikator wiadomosci
            String qualifier = message.getStringProperty("QUAL");
            String value = message.getStringProperty("VAL");
            Date now = new Date();

            // Q_OK = odpowiedz na Q
            if ("Q_OK".equalsIgnoreCase(qualifier)) {
                return null; // brak odpowiedzi!
            }
            
            // PING_OK = odpowiedz na PING
            if ("PING_OK".equalsIgnoreCase(qualifier)) {
                ManagerLocation manager = CloudBurst.findManagerByURI(value);
                if (manager != null)
                    manager.setLastConfirmationAt(now.getTime());
                return null; // brak odpowiedzi!
            }

            // REG_OK = odpowiedz na REG, rejestracja pomyslna
            if ("REG_OK".equalsIgnoreCase(qualifier)) {
                return null; // brak odpowiedzi!
            }
            // REG = rejestracja
            if ("REG".equalsIgnoreCase(qualifier)) {
                CloudBurst.addNode(value, true); // dodaj managera do listy i przekaz te wiadomosc wszystkim znanym managerom
                response = session.createMapMessage();
                response.setStringProperty("QUAL", "REG_OK"); // REG ACCEPTED
                response.setStringProperty("VAL", CloudBurst.getMyUri()); //
                return response;
            }
            
            // PING = test połączenia
            if ("PING".equalsIgnoreCase(qualifier)) {
                response = session.createMapMessage();
                response.setStringProperty("QUAL", "PING_OK"); // REG ACCEPTED
                response.setStringProperty("VAL", CloudBurst.getMyUri()); //
                return response;
            }

            // Q = zapytanie
            if ("Q".equalsIgnoreCase(qualifier)) {
                response = session.createMapMessage();
                response.setStringProperty("QUAL", "Q_OK");
                response.setStringProperty("VAL", "ACK"); // ACK = OK
                return response;
            } else {
                response = session.createMapMessage();
                response.setStringProperty("QUAL", "Q_FAIL");
                response.setStringProperty("VAL", "NEG"); // NEG = FAILURE
                return response;
            }

        } catch (JMSException ex) {
            Logger.getLogger(MessageProtocol.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
