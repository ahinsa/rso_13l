package rso.manager;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

/**
 * Klasa odpowiedzialna za procesowanie wiadomosci przychodzacych
 */
public class Receiver extends Thread {

    private boolean bRunning = false;
    private boolean bStopped = false;
    MessageProtocol msgProtocol = null;

    public void run() {
        System.out.println("Receiver() starting");
        bRunning = true;

        Connection connection = null;
        try {
            ConnectionFactory factory = CloudBurst.getJmsConnectionFactory();
            connection = factory.createConnection();
            connection.start();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
            Queue queue = session.createQueue(CloudBurst.QUEUE1);

            MessageConsumer consumer = session.createConsumer(queue);
            // odpowiedz zwrotna
            Message replyMsg = null;
            msgProtocol = new MessageProtocol();

            while (bRunning) {
                Message message = consumer.receive(1000);
                if (message instanceof MapMessage) {
                    String val = ((MapMessage) message).getStringProperty("VAL");
                    System.out.println("[Receiver] received message: QUAL=" + message.getStringProperty("QUAL") + " VAL=" + message.getStringProperty("VAL"));
                    // wyslanie odpowiedzi
                    if (message.getJMSReplyTo() != null) {
                        MessageProducer replyProducer = session.createProducer(message.getJMSReplyTo());
                        replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

                        replyMsg = this.msgProtocol.handleProtocolMessage(session, message);
                        if (replyMsg != null) {
                            replyProducer.send(replyMsg);
                            System.out.println("[Reply] response message: QUAL=" + replyMsg.getStringProperty("QUAL") + " VAL=" + replyMsg.getStringProperty("VAL"));
                        }
                    }
                }
            }
            // brak możliwości połączenia z managerem
        } catch (JMSException e) {
            System.out.println("Exception occurred: " + e.toString());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
            }
            System.out.println("Receiver ended");
            bStopped = true;
            synchronized (this) {
                notifyAll();
            }
        }
    }

    synchronized public void stopListening() {
        System.out.println("Receiver.stopListening()");
        bRunning = false;
        while (!bStopped) {
            System.out.println("Receiver.stopListening().Waiting for connections to close...");
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
