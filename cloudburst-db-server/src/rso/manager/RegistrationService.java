package rso.manager;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RegistrationService extends Remote {
    public void registerNode(String url) throws RemoteException;
}
