package rso.manager;

import rso.driver.remote.RemoteDriverImpl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class RegistrationServiceImpl extends UnicastRemoteObject implements RegistrationService {

    protected RegistrationServiceImpl() throws RemoteException {
        super();
    }

    @Override
    public void registerNode(String url) throws RemoteException {
        new RemoteDriverImpl().registerNode(url);
    }
}
