package rso.manager;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * Klasa odpowiedzialna za procesowanie wiadomosci wychodzacych
 */
public class Sender {

    static public MapMessage sendMessage(String qualifier, String value, String brokerURL, String queue) {
        ConnectionFactory factory = null;
        Connection connection = null;
        MapMessage replyMsg = null;

        try {
            factory = new ActiveMQConnectionFactory(brokerURL);
            connection = factory.createConnection();
            connection.start();

            Session session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
            
            Queue sendQueue = session.createQueue(queue);
            MessageProducer producer = session.createProducer(sendQueue);

            // kolejka dla wiadomosci zwrotnych (odpowiedzi)
            Queue responseQueue = session.createTemporaryQueue();
            MessageConsumer consumer = session.createConsumer(responseQueue);

            producer.setTimeToLive(500); // ważność wiadomości 0,5 sec
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            MapMessage msg = session.createMapMessage();
            msg.setStringProperty("QUAL", qualifier);
            msg.setStringProperty("VAL", value);
            msg.setJMSReplyTo(responseQueue);

            System.out.println("[Sender] sending message: QUAL=" + msg.getStringProperty("QUAL") + " VAL=" + msg.getStringProperty("VAL"));

            producer.send(msg);
            // sprawdzenie czy w buforze istnieje wiadomosc zwrotna
            MapMessage replyMessage = (MapMessage) consumer.receive(1000);

            session.close();

            return replyMessage;
        } catch (Exception e) {
            System.out.println("Exception occurred: " + e.toString());
            return null;

        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                }
            }
        }
    }
}
