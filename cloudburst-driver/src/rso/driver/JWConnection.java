/**
 * JWConnection - Klasa implementuje interfejs java.sql.Connection i pełni
 * rolę Connection po stronie klienta. Komunikuje się z Remote Connection.
 */

package rso.driver;

import rso.driver.remote.IRemoteConnection;
import rso.driver.remote.IRemoteStatement;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

public class JWConnection implements Connection {
    //Remote connection
    private IRemoteConnection remoteConnection;

    /**
     * constructor for creating the JWConnection instance with IRemoteConnection
     */
    public JWConnection(IRemoteConnection remCon) {
        remoteConnection = remCon;
    }


    /**
     * This method creates a remote statement and then returns JWStatement
     * object holding the remote statement
     */
    @Override
    public Statement createStatement()
            throws SQLException {
        try {
            IRemoteStatement remStmt = remoteConnection.createStatement();
            JWStatement localStmtInstance = new JWStatement(remStmt);
            return localStmtInstance;

        } catch (Exception ex) {
            throw (new SQLException("RemoteException:" + ex.getMessage()));
        }
    }

    /**
     * This method closes the remote Connection
     */
    @Override
    public void close() throws SQLException {
        try {
            remoteConnection.close();
        } catch (Exception ex) {
            throw ((new SQLException("RemoteException:" + ex.getMessage())));
        }
    }

    //////////////////////////////////////////////////////////////////////////////
    //Not supported methods
    @Override
    public String nativeSQL(String sql) throws SQLException {
        try {
            return remoteConnection.nativeSQL(sql);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setAutoCommit(boolean autoCommit)
            throws SQLException {
        try {
            remoteConnection.setAutoCommit(autoCommit);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public boolean getAutoCommit()
            throws SQLException {
        try {
            return remoteConnection.getAutoCommit();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void commit() throws SQLException {
        try {
            remoteConnection.commit();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void rollback() throws SQLException {
        try {
            remoteConnection.rollback();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public boolean isClosed() throws SQLException {
        try {
            return remoteConnection.isClosed();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public DatabaseMetaData getMetaData()
            throws SQLException {
        try {
            return remoteConnection.getMetaData();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setReadOnly(boolean readOnly)
            throws SQLException {
        try {
            remoteConnection.setReadOnly(readOnly);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public boolean isReadOnly()
            throws SQLException {
        try {
            return remoteConnection.isReadOnly();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setCatalog(String catalog)
            throws SQLException {
        try {
            remoteConnection.setCatalog(catalog);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public String getCatalog()
            throws SQLException {
        try {
            return remoteConnection.getCatalog();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setTransactionIsolation(int level)
            throws SQLException {
        try {
            remoteConnection.setTransactionIsolation(level);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public int getTransactionIsolation()
            throws SQLException {
        try {
            return remoteConnection.getTransactionIsolation();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public SQLWarning getWarnings()
            throws SQLException {
        try {
            return remoteConnection.getWarnings();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void clearWarnings()
            throws SQLException {
        try {
            remoteConnection.clearWarnings();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public PreparedStatement prepareStatement(String sql)
            throws SQLException {
        throw (new SQLException("Not Supported"));
    }

    @Override
    public CallableStatement prepareCall(String sql)
            throws SQLException {
        throw (new SQLException("Not Supported"));
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
            throws SQLException {
        throw (new SQLException("Not Supported"));
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
            throws SQLException {
        throw (new SQLException("Not Supported"));
    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency)
            throws SQLException {
        try {
            JWStatement localStmtInstance = new JWStatement(remoteConnection.createStatement(resultSetType, resultSetConcurrency));
            return localStmtInstance;
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    /////////////////////////////////////////////////////////////
    //These method would have to be implemented for JDK1.2 and higher

    @Override
    public void setTypeMap(Map map) throws SQLException {
       try {
            remoteConnection.setTypeMap(map);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Map getTypeMap() throws SQLException {
        try {
            return remoteConnection.getTypeMap();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setHoldability(int holdability) throws SQLException {
          try {
            remoteConnection.setHoldability(holdability);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public int getHoldability() throws SQLException {
          try {
            return remoteConnection.getHoldability();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Savepoint setSavepoint() throws SQLException {
          try {
            return remoteConnection.setSavepoint();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Savepoint setSavepoint(String name) throws SQLException {
          try {
            return remoteConnection.setSavepoint(name);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void rollback(Savepoint savepoint) throws SQLException {
         try {
            remoteConnection.rollback(savepoint);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
         try {
            remoteConnection.releaseSavepoint(savepoint);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
         try {
             JWStatement localStmtInstance = new JWStatement(remoteConnection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability));
             return localStmtInstance;
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Clob createClob() throws SQLException {
          try {
            return remoteConnection.createClob();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Blob createBlob() throws SQLException {
         try {
            return remoteConnection.createBlob();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public NClob createNClob() throws SQLException {
          try {
            return remoteConnection.createNClob();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
          try {
            return remoteConnection.createSQLXML();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public boolean isValid(int timeout) throws SQLException {
         try {
            return remoteConnection.isValid(timeout);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setClientInfo(String name, String value) throws SQLClientInfoException {
          try {
            remoteConnection.setClientInfo(name, value);
        } catch (Exception ex) {
            throw new SQLClientInfoException();//"RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {
          try {
            remoteConnection.setClientInfo(properties);
        } catch (Exception ex) {
            throw new SQLClientInfoException();
        }
    }

    @Override
    public String getClientInfo(String name) throws SQLException {
         try {
            return remoteConnection.getClientInfo(name);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Properties getClientInfo() throws SQLException {
          try {
            return remoteConnection.getClientInfo();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
         try {
            return remoteConnection.createArrayOf(typeName, elements);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
          try {
            return remoteConnection.createStruct(typeName, attributes);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setSchema(String schema) throws SQLException {
          try {
            remoteConnection.setSchema(schema);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public String getSchema() throws SQLException {
         try {
            return remoteConnection.getSchema();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void abort(Executor executor) throws SQLException {
         try {
            remoteConnection.abort(executor);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
          try {
            remoteConnection.setNetworkTimeout(executor, milliseconds);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public int getNetworkTimeout() throws SQLException {
         try {
            return remoteConnection.getNetworkTimeout();
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
          try {
            return remoteConnection.unwrap(iface);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
          try {
            return remoteConnection.isWrapperFor(iface);
        } catch (Exception ex) {
            throw new SQLException("RemoteException:" + ex.getMessage());
        }
    }
}







