/**
 * JWDriver - Klasa implementuje interfejs java.sql.Driver i pełni rolę
 * Driver'a po stronie klienta. Komunikuje się z Remote Driver.
 *
 * URL sterownika to jdbc:JWDriver: + <Remote server name/IP Address>.
 */

package rso.driver;

import rso.driver.remote.IRemoteConnection;
import rso.driver.remote.IRemoteDriver;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

public class JWDriver implements java.sql.Driver {
    //Remote driver
    static IRemoteDriver remoteDriver = null;

    //Driver URL prefix.
    private static final String URL_PREFIX = "jdbc:JWDriver:";

    private static final int MAJOR_VERSION = 1;
    private static final int MINOR_VERSION = 0;

    static {
        try {
            //Register the JWDriver with DriverManager
            JWDriver driverInst = new JWDriver();
            DriverManager.registerDriver(driverInst);

            //System.setSecurityManager(new RMISecurityManager());
        } catch (Exception e) {
        }
    }

    /**
     * It returns the URL prefix for using JWDriver
     */
    public static String getURLPrefix() {
        return URL_PREFIX;
    }

    /**
     * This method create a remote connection and then
     * returns the JWConnection object holdng a Remote Connection
     */
    @Override
    public Connection connect(String url, Properties loginProp)
            throws SQLException {
        JWConnection localConInstance = null;

        if (acceptsURL(url)) {
            try {
                //extract the remote server location coming with URL
                String serverName = url.substring(URL_PREFIX.length(), url.length());


                //Get the remote Connection
                String username = loginProp.getProperty("user");
                String password = loginProp.getProperty("password");
                String dbName = loginProp.getProperty("databaseName");
                boolean fromClient;
                if (url.charAt(0) == 'x') {
                    fromClient = false;
                    serverName = url.substring(URL_PREFIX.length()+1, url.length());
                } else {
                    fromClient = true;
                }
                //connect to remote server only if not already connected
                connectRemote(serverName);

                IRemoteConnection remoteConInstance = remoteDriver.getConnection(username,password,dbName, fromClient);

                //Create the local JWConnection holding remote Connenction
                localConInstance = new JWConnection(remoteConInstance);

            } catch (RemoteException ex) {
                throw (new SQLException(ex.getMessage()));
            } catch (Exception ex) {
                throw (new SQLException(ex.getMessage()));
            }
        }

        return localConInstance;
    }

    /**
     * This method makes the one time connection to the RMI server
     */
    private void connectRemote(String serverName) throws Exception {
        try {
            if (remoteDriver == null) {
                remoteDriver = (IRemoteDriver) Naming.lookup("rmi://" + serverName + ":1099" + "/RemoteDriver");
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * This method returns true if the given URL starts with the JWDriver url.
     * It is called by DriverManager.
     */
    @Override
    public boolean acceptsURL(String url)
            throws SQLException {
        return url.startsWith(URL_PREFIX) || url.startsWith("x"+URL_PREFIX);
    }

    @Override
    public int getMajorVersion() {
        return MAJOR_VERSION;
    }

    @Override
    public int getMinorVersion() {
        return MINOR_VERSION;
    }

    @Override
    public java.sql.DriverPropertyInfo[] getPropertyInfo(String url, Properties loginProps)
            throws SQLException {
        return new DriverPropertyInfo[0];
    }

    @Override
    public boolean jdbcCompliant() {
        return false;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}