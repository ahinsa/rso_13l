/**
 * JWResultSet - Klasa implementuje java.sql.ResultSet. Pełni rolę ResultSet
 * po stronie klienta. Komunikuje się z Remote ResultSet.
 */


package rso.driver;

import rso.driver.remote.IRemoteResultSet;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.rmi.RemoteException;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

public class JWResultSet implements java.sql.ResultSet {
    //Remote ResultSet
    private IRemoteResultSet remoteResultSet;

    /**
     * Constructor for creating the JWResultSet with IRemoteResultSet
     */
    public JWResultSet(IRemoteResultSet rsInstance) throws SQLException {
        remoteResultSet = rsInstance;
    }

    @Override
    public boolean next() throws SQLException {
        try {
            return remoteResultSet.next();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void close() throws SQLException {
        try {
            remoteResultSet.close();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean wasNull() throws SQLException {
        try {
            return remoteResultSet.wasNull();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public String getString(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getString(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }

    }

    @Override
    public boolean getBoolean(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getBoolean(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public byte getByte(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getByte(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public short getShort(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getShort(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getInt(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getInt(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public long getLong(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getLong(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public float getFloat(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getFloat(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public double getDouble(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getDouble(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
        try {
            return remoteResultSet.getBigDecimal(columnIndex, scale);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public byte[] getBytes(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getBytes(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Date getDate(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getDate(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Time getTime(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getTime(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getTimestamp(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public InputStream getAsciiStream(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getAsciiStream(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public InputStream getUnicodeStream(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getUnicodeStream(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public InputStream getBinaryStream(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getBinaryStream(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public String getString(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getString(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean getBoolean(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getBoolean(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public byte getByte(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getByte(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public short getShort(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getShort(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getInt(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getInt(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public long getLong(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getLong(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public float getFloat(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getFloat(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public double getDouble(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getDouble(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {
        return getBigDecimal(columnLabel, scale);
    }

    @Override
    public byte[] getBytes(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getBytes(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Date getDate(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getDate(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Time getTime(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getTime(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getTimestamp(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public InputStream getAsciiStream(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getAsciiStream(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public InputStream getUnicodeStream(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getUnicodeStream(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public InputStream getBinaryStream(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getBinaryStream(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        try {
            return remoteResultSet.getWarnings();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void clearWarnings() throws SQLException {
        try {
            remoteResultSet.clearWarnings();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public String getCursorName() throws SQLException {
        try {
            return remoteResultSet.getCursorName();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        try {
            return remoteResultSet.getMetaData();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Object getObject(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getObject(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Object getObject(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getObject(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int findColumn(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.findColumn(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Reader getCharacterStream(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getCharacterStream(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Reader getCharacterStream(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getCharacterStream(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getBigDecimal(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getBigDecimal(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean isBeforeFirst() throws SQLException {
        try {
            return remoteResultSet.isBeforeFirst();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean isAfterLast() throws SQLException {
        try {
            return remoteResultSet.isAfterLast();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean isFirst() throws SQLException {
        try {
            return remoteResultSet.isFirst();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean isLast() throws SQLException {
        try {
            return remoteResultSet.isLast();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void beforeFirst() throws SQLException {
        try {
            remoteResultSet.beforeFirst();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void afterLast() throws SQLException {
        try {
            remoteResultSet.afterLast();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean first() throws SQLException {
        try {
            return remoteResultSet.first();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean last() throws SQLException {
        try {
            return remoteResultSet.last();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getRow() throws SQLException {
        try {
            return remoteResultSet.getRow();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean absolute(int row) throws SQLException {
        try {
            return remoteResultSet.absolute(row);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean relative(int rows) throws SQLException {
        try {
            return remoteResultSet.relative(rows);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean previous() throws SQLException {
        try {
            return remoteResultSet.previous();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void setFetchDirection(int direction) throws SQLException {
        try {
            remoteResultSet.setFetchDirection(direction);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getFetchDirection() throws SQLException {
        try {
            return remoteResultSet.getFetchDirection();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void setFetchSize(int rows) throws SQLException {
        try {
            remoteResultSet.setFetchSize(rows);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getFetchSize() throws SQLException {
        try {
            return remoteResultSet.getFetchSize();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getType() throws SQLException {
        try {
            return remoteResultSet.getType();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getConcurrency() throws SQLException {
        try {
            return remoteResultSet.getConcurrency();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean rowUpdated() throws SQLException {
        try {
            return remoteResultSet.rowUpdated();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean rowInserted() throws SQLException {
        try {
            return remoteResultSet.rowInserted();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean rowDeleted() throws SQLException {
        try {
            return remoteResultSet.rowDeleted();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNull(int columnIndex) throws SQLException {
        try {
            remoteResultSet.updateNull(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBoolean(int columnIndex, boolean x) throws SQLException {
        try {
            remoteResultSet.updateBoolean(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateByte(int columnIndex, byte x) throws SQLException {
        try {
            remoteResultSet.updateByte(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateShort(int columnIndex, short x) throws SQLException {
        try {
            remoteResultSet.updateShort(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateInt(int columnIndex, int x) throws SQLException {
        try {
            remoteResultSet.updateInt(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateLong(int columnIndex, long x) throws SQLException {
        try {
            remoteResultSet.updateLong(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateFloat(int columnIndex, float x) throws SQLException {
        try {
            remoteResultSet.updateFloat(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateDouble(int columnIndex, double x) throws SQLException {
        try {
            remoteResultSet.updateDouble(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
        try {
            remoteResultSet.updateBigDecimal(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateString(int columnIndex, String x) throws SQLException {
        try {
            remoteResultSet.updateString(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBytes(int columnIndex, byte[] x) throws SQLException {
        try {
            remoteResultSet.updateBytes(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateDate(int columnIndex, Date x) throws SQLException {
        try {
            remoteResultSet.updateDate(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateTime(int columnIndex, Time x) throws SQLException {
        try {
            remoteResultSet.updateTime(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
        try {
            remoteResultSet.updateTimestamp(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
        try {
            remoteResultSet.updateAsciiStream(columnIndex,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
        try {
            remoteResultSet.updateBinaryStream(columnIndex,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {
        try {
            remoteResultSet.updateCharacterStream(columnIndex,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException {
        try {
            remoteResultSet.updateObject(columnIndex,x,scaleOrLength);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateObject(int columnIndex, Object x) throws SQLException {
        try {
            remoteResultSet.updateObject(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNull(String columnLabel) throws SQLException {
        try {
            remoteResultSet.updateNull(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBoolean(String columnLabel, boolean x) throws SQLException {
        try {
            remoteResultSet.updateBoolean(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateByte(String columnLabel, byte x) throws SQLException {
        try {
            remoteResultSet.updateByte(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateShort(String columnLabel, short x) throws SQLException {
        try {
            remoteResultSet.updateShort(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateInt(String columnLabel, int x) throws SQLException {
        try {
            remoteResultSet.updateInt(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateLong(String columnLabel, long x) throws SQLException {
        try {
            remoteResultSet.updateLong(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateFloat(String columnLabel, float x) throws SQLException {
        try {
            remoteResultSet.updateFloat(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateDouble(String columnLabel, double x) throws SQLException {
        try {
            remoteResultSet.updateDouble(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {
        try {
            remoteResultSet.updateBigDecimal(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateString(String columnLabel, String x) throws SQLException {
        try {
            remoteResultSet.updateString(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBytes(String columnLabel, byte[] x) throws SQLException {
        try {
            remoteResultSet.updateBytes(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateDate(String columnLabel, Date x) throws SQLException {
        try {
            remoteResultSet.updateDate(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateTime(String columnLabel, Time x) throws SQLException {
        try {
            remoteResultSet.updateTime(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateTimestamp(String columnLabel, Timestamp x) throws SQLException {
        try {
            remoteResultSet.updateTimestamp(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException {
        try {
            remoteResultSet.updateAsciiStream(columnLabel,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, int length) throws SQLException {
        try {
            remoteResultSet.updateBinaryStream(columnLabel,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, int length) throws SQLException {
        try {
            remoteResultSet.updateCharacterStream(columnLabel,reader,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException {
        try {
            remoteResultSet.updateObject(columnLabel,x,scaleOrLength);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateObject(String columnLabel, Object x) throws SQLException {
        try {
            remoteResultSet.updateObject(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void insertRow() throws SQLException {
        try {
            remoteResultSet.insertRow();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateRow() throws SQLException {
        try {
            remoteResultSet.updateRow();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void deleteRow() throws SQLException {
        try {
            remoteResultSet.insertRow();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void refreshRow() throws SQLException {
        try {
            remoteResultSet.refreshRow();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void cancelRowUpdates() throws SQLException {
        try {
            remoteResultSet.cancelRowUpdates();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void moveToInsertRow() throws SQLException {
        try {
            remoteResultSet.moveToInsertRow();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void moveToCurrentRow() throws SQLException {
        try {
            remoteResultSet.moveToCurrentRow();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Statement getStatement() throws SQLException {
        try {
            return remoteResultSet.getStatement();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {
        try {
            return remoteResultSet.getObject(columnIndex,map);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Ref getRef(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getRef(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Blob getBlob(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getBlob(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Clob getClob(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getClob(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Array getArray(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getArray(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
        try {
            return remoteResultSet.getObject(columnLabel,map);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Ref getRef(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getRef(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Blob getBlob(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getBlob(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Clob getClob(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getClob(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Array getArray(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getArray(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Date getDate(int columnIndex, Calendar cal) throws SQLException {
        try {
            return remoteResultSet.getDate(columnIndex,cal);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Date getDate(String columnLabel, Calendar cal) throws SQLException {
        try {
            return remoteResultSet.getDate(columnLabel,cal);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Time getTime(int columnIndex, Calendar cal) throws SQLException {
        try {
            return remoteResultSet.getTime(columnIndex,cal);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Time getTime(String columnLabel, Calendar cal) throws SQLException {
        try {
            return remoteResultSet.getTime(columnLabel,cal);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
        try {
            return remoteResultSet.getTimestamp(columnIndex,cal);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
        try {
            return remoteResultSet.getTimestamp(columnLabel,cal);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public URL getURL(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getURL(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public URL getURL(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getURL(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateRef(int columnIndex, Ref x) throws SQLException {
        try {
            remoteResultSet.updateRef(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateRef(String columnLabel, Ref x) throws SQLException {
        try {
            remoteResultSet.updateRef(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(int columnIndex, Blob x) throws SQLException {
        try {
            remoteResultSet.updateBlob(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(String columnLabel, Blob x) throws SQLException {
        try {
            remoteResultSet.updateBlob(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateClob(int columnIndex, Clob x) throws SQLException {
        try {
            remoteResultSet.updateClob(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateClob(String columnLabel, Clob x) throws SQLException {
        try {
            remoteResultSet.updateClob(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateArray(int columnIndex, Array x) throws SQLException {
        try {
            remoteResultSet.updateArray(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateArray(String columnLabel, Array x) throws SQLException {
        try {
            remoteResultSet.updateArray(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public RowId getRowId(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getRowId(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public RowId getRowId(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getRowId(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateRowId(int columnIndex, RowId x) throws SQLException {
        try {
            remoteResultSet.updateRowId(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateRowId(String columnLabel, RowId x) throws SQLException {
        try {
            remoteResultSet.updateRowId(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int getHoldability() throws SQLException {
        try {
            return remoteResultSet.getHoldability();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean isClosed() throws SQLException {
        try {
            return remoteResultSet.isClosed();
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNString(int columnIndex, String nString) throws SQLException {
        try {
            remoteResultSet.updateNString(columnIndex,nString);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNString(String columnLabel, String nString) throws SQLException {
        try {
            remoteResultSet.updateNString(columnLabel,nString);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
        try {
            remoteResultSet.updateNClob(columnIndex,nClob);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(String columnLabel, NClob nClob) throws SQLException {
        try {
            remoteResultSet.updateNClob(columnLabel,nClob);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public NClob getNClob(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getNClob(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public NClob getNClob(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getNClob(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public SQLXML getSQLXML(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getSQLXML(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public SQLXML getSQLXML(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getSQLXML(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
        try {
            remoteResultSet.updateSQLXML(columnIndex,xmlObject);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
        try {
            remoteResultSet.updateSQLXML(columnLabel,xmlObject);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public String getNString(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getNString(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public String getNString(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getNString(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Reader getNCharacterStream(int columnIndex) throws SQLException {
        try {
            return remoteResultSet.getNCharacterStream(columnIndex);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public Reader getNCharacterStream(String columnLabel) throws SQLException {
        try {
            return remoteResultSet.getNCharacterStream(columnLabel);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
        try {
            remoteResultSet.updateNCharacterStream(columnIndex,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
        try {
            remoteResultSet.updateNCharacterStream(columnLabel,reader,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
        try {
            remoteResultSet.updateAsciiStream(columnIndex,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
        try {
            remoteResultSet.updateBinaryStream(columnIndex,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
        try {
            remoteResultSet.updateCharacterStream(columnIndex,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
        try {
            remoteResultSet.updateAsciiStream(columnLabel,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
        try {
            remoteResultSet.updateBinaryStream(columnLabel,x,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
        try {
            remoteResultSet.updateCharacterStream(columnLabel,reader,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
        try {
            remoteResultSet.updateBlob(columnIndex,inputStream,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
        try {
            remoteResultSet.updateBlob(columnLabel,inputStream,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
        try {
            remoteResultSet.updateClob(columnIndex,reader,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
        try {
            remoteResultSet.updateClob(columnLabel,reader,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
        try {
            remoteResultSet.updateClob(columnIndex,reader,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
        try {
            remoteResultSet.updateNClob(columnLabel,reader,length);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
        try {
            remoteResultSet.updateNCharacterStream(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
        try {
            remoteResultSet.updateNCharacterStream(columnLabel,reader);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
        try {
            remoteResultSet.updateAsciiStream(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
        try {
            remoteResultSet.updateBinaryStream(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
        try {
            remoteResultSet.updateCharacterStream(columnIndex,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
        try {
            remoteResultSet.updateAsciiStream(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
        try {
            remoteResultSet.updateBinaryStream(columnLabel,x);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
        try {
            remoteResultSet.updateClob(columnLabel,reader);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
        try {
            remoteResultSet.updateBlob(columnIndex,inputStream);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
        try {
            remoteResultSet.updateBlob(columnLabel,inputStream);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateClob(int columnIndex, Reader reader) throws SQLException {
        try {
            remoteResultSet.updateClob(columnIndex,reader);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateClob(String columnLabel, Reader reader) throws SQLException {
        try {
            remoteResultSet.updateClob(columnLabel,reader);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(int columnIndex, Reader reader) throws SQLException {
        try {
            remoteResultSet.updateNClob(columnIndex,reader);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void updateNClob(String columnLabel, Reader reader) throws SQLException {
        try {
            remoteResultSet.updateNClob(columnLabel,reader);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
        try {
            return remoteResultSet.getObject(columnIndex,type);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
        try {
            return remoteResultSet.getObject(columnLabel,type);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        try {
            return remoteResultSet.unwrap(iface);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        try {
            return remoteResultSet.isWrapperFor(iface);
        } catch (RemoteException e) {
            throw new SQLException(e.getMessage());
        }
    }
}