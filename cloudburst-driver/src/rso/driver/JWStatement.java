/**
 * JWStatement - Implementuje java.sql.Statement. Klasa pełni rolę 
 * Statement po stronie klienta. Komunikuje się z Remote Statement.
 */

package rso.driver;

import rso.driver.remote.IRemoteStatement;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;

public class JWStatement implements java.sql.Statement {
    //Remote Statement
    private IRemoteStatement remoteStmt;

    /**
     * Constructor for creating the JWStatement
     *
     * @param stmt
     */
    public JWStatement(IRemoteStatement stmt) {
        remoteStmt = stmt;
    }

    @Override
    public ResultSet executeQuery(String sql) throws SQLException {
        try {
            return new JWResultSet(remoteStmt.executeQuery(sql));
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int executeUpdate(String sql) throws SQLException {
        try {
            return remoteStmt.executeUpdate(sql);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void close() throws SQLException {
        try {
            remoteStmt.close();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getMaxFieldSize() throws SQLException {
        try {
            return remoteStmt.getMaxFieldSize();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setMaxFieldSize(int max) throws SQLException {
        try {
            remoteStmt.setMaxFieldSize(max);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getMaxRows() throws SQLException {
        try {
            return remoteStmt.getMaxRows();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setMaxRows(int max) throws SQLException {
        try {
            remoteStmt.setMaxRows(max);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setEscapeProcessing(boolean enable) throws SQLException {
        try {
            remoteStmt.setEscapeProcessing(enable);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getQueryTimeout() throws SQLException {
        try {
            return remoteStmt.getQueryTimeout();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setQueryTimeout(int seconds) throws SQLException {
        try {
            remoteStmt.setQueryTimeout(seconds);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void cancel() throws SQLException {
        try {
            remoteStmt.cancel();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        try {
            return remoteStmt.getWarnings();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void clearWarnings() throws SQLException {
        try {
            remoteStmt.clearWarnings();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setCursorName(String name) throws SQLException {
        try {
            remoteStmt.setCursorName(name);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean execute(String sql) throws SQLException {
        try {
            return remoteStmt.execute(sql);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        try {
            return remoteStmt.getResultSet();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getUpdateCount() throws SQLException {
        try {
            return remoteStmt.getUpdateCount();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean getMoreResults() throws SQLException {
        try {
            return remoteStmt.getMoreResults();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setFetchDirection(int direction) throws SQLException {
        try {
            remoteStmt.setFetchDirection(direction);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getFetchDirection() throws SQLException {
        try {
            return remoteStmt.getFetchDirection();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setFetchSize(int rows) throws SQLException {
        try {
            remoteStmt.setFetchSize(rows);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getFetchSize() throws SQLException {
        try {
            return remoteStmt.getFetchSize();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getResultSetConcurrency() throws SQLException {
        try {
            return remoteStmt.getResultSetConcurrency();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getResultSetType() throws SQLException {
        try {
            return remoteStmt.getResultSetType();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void addBatch(String sql) throws SQLException {
        try {
            remoteStmt.addBatch(sql);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void clearBatch() throws SQLException {
        try {
            remoteStmt.clearBatch();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int[] executeBatch() throws SQLException {
        try {
            return remoteStmt.executeBatch();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        try {
            return remoteStmt.getConnection();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean getMoreResults(int current) throws SQLException {
        try {
            return remoteStmt.getMoreResults();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        try {
            return remoteStmt.getGeneratedKeys();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
        try {
            return remoteStmt.executeUpdate(sql,autoGeneratedKeys);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
        try {
            return remoteStmt.executeUpdate(sql,columnIndexes);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int executeUpdate(String sql, String[] columnNames) throws SQLException {
        try {
            return remoteStmt.executeUpdate(sql,columnNames);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
        try {
            return remoteStmt.execute(sql,autoGeneratedKeys);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean execute(String sql, int[] columnIndexes) throws SQLException {
        try {
            return remoteStmt.execute(sql,columnIndexes);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean execute(String sql, String[] columnNames) throws SQLException {
        try {
            return remoteStmt.execute(sql,columnNames);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getResultSetHoldability() throws SQLException {
        try {
            return remoteStmt.getResultSetHoldability();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean isClosed() throws SQLException {
        try {
            return remoteStmt.isClosed();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void setPoolable(boolean poolable) throws SQLException {
        try {
            remoteStmt.setPoolable(poolable);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean isPoolable() throws SQLException {
        try {
            return remoteStmt.isPoolable();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        try {
            remoteStmt.closeOnCompletion();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        try {
            return remoteStmt.isCloseOnCompletion();
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        try {
            return remoteStmt.unwrap(iface);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        try {
            return remoteStmt.isWrapperFor(iface);
        } catch (RemoteException e) {
            throw new SQLException(e);
        }
    }
}
