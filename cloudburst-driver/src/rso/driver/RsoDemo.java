package rso.driver;

import java.sql.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class RsoDemo {

    public static void main(String[] args) {
//        try {
//            Class.forName("rso.driver.JWDriver");
//            Connection connection = DriverManager.getConnection("jdbc:JWDriver:localhost");
//            Statement statement = connection.createStatement();
//            statement.execute("create table dummy (dummy_id int, dummy_name varchar(20))");
//
//            System.out.println("insert into dummy(dummy_id, dummy_name) values(1, 'name')");
//
//            statement.execute("insert into dummy(dummy_id, dummy_name) values(1, 'name')");
//
//            ResultSet resultSet = statement.executeQuery("select * from dummy");
//            resultSet.next();
//            int dummy_id = resultSet.getInt(1);
//            String dummy_name = resultSet.getString(2);
//            System.out.println("Dummy_id from db: " + dummy_id);
//            System.out.println("Dummy_name from db: " + dummy_name);
//
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }

        String filePath = "scripts/src/insert.sql";

        createTables();
        System.out.println("\nTEST WYDAJNOŚCIOWY");
        insertPerformanceTest(filePath);
        selectPerformenceTest("select * from purchase");
        selectPerformenceTest("select * from purchase where id_customer = 7");

        System.out.println("\nTEST POPRAWNOŚCIOWY");
        insertCorrectnessTest(filePath);
        dropTables();
    }

    private static void dropTables() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:JWDriver:localhost");
            Statement statement = connection.createStatement();

            statement.execute("DROP TABLE customer");
            statement.execute("DROP TABLE product");
            statement.execute("DROP TABLE purchase");
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private static void createTables()
    {
        try {
            Class.forName("rso.driver.JWDriver");
            Connection connection = DriverManager.getConnection("jdbc:JWDriver:localhost");
            Statement statement = connection.createStatement();

            statement.execute("CREATE TABLE customer (id INT, name VARCHAR(20), surname VARCHAR(32), city VARCHAR(16), street VARCHAR(16), nr INT)");
            statement.execute("CREATE TABLE product (id INT, name VARCHAR(20), producer VARCHAR(20), price INT, weight INT)");
            statement.execute("CREATE TABLE purchase (id INT, id_customer INT, id_product INT, quantity INT)");

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void insertPerformanceTest(String filePath)
    {
        long startTime, endTime;
        int counter = 0;
        FileReader fileReader;
        BufferedReader bufferedReader = null;
        ArrayList<String> listInsert = new ArrayList<String>();


        startTime = System.currentTimeMillis();
        try
        {
            Class.forName("rso.driver.JWDriver");
            Connection connection = DriverManager.getConnection("jdbc:JWDriver:localhost");
            Statement statement = connection.createStatement();
            fileReader = new FileReader(filePath);
            bufferedReader = new BufferedReader(fileReader);
            String textLine = bufferedReader.readLine();
            do
            {
                textLine = bufferedReader.readLine();
                if(textLine!=null)
                    if(textLine.startsWith("INSERT"))
                    {
//						System.out.println(textLine);
//                      listInsert.add(textLine);
                        statement.execute(textLine);
                        counter++;
                    }
            }
            while (textLine != null);
        }
        catch (IOException | SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        endTime = System.currentTimeMillis();
        System.out.println("Wstawianie " + counter + " rekordów w 3 tabelach: " + (endTime-startTime) + "ms.\n");

    }

    private static void selectPerformenceTest(String query)
    {
        long startTime, endTime;
        ResultSet purchaseResultSet = null;
        startTime = System.currentTimeMillis();

        try {
            Class.forName("rso.driver.JWDriver");
            Connection connection = DriverManager.getConnection("jdbc:JWDriver:localhost");
            Statement statement = connection.createStatement();
            purchaseResultSet = statement.executeQuery(query);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        endTime = System.currentTimeMillis();

        int rsCount = 0;
        try {
            while(purchaseResultSet.next())
                rsCount = rsCount + 1;
        } catch (SQLException e){
            e.printStackTrace();
        }

        System.out.println("Zapytanie: " + query + "\n");
        System.out.println("Odczytano " + rsCount + " rekordów w czasie " + (endTime-startTime) + "ms.\n");
    }

    private static void insertCorrectnessTest(String filePath)
    {
        int customerCounter = 0;
        int productCounter = 0;
        int purchaseCounter = 0;
        ArrayList<String> customerInsertList = new ArrayList<String>();
        customerInsertList = prepareInsertCheckList(filePath, "customer");
        ArrayList<String> productInsertList = new ArrayList<String>();
        productInsertList = prepareInsertCheckList(filePath, "product");
        ArrayList<String> purchaseInsertList = new ArrayList<String>();
        purchaseInsertList = prepareInsertCheckList(filePath, "purchase");

        try {
            Class.forName("rso.driver.JWDriver");
            Connection connection = DriverManager.getConnection("jdbc:JWDriver:localhost");
            Statement statement = connection.createStatement();

            ResultSet customerResultSet = statement.executeQuery("select * from customer");
            String customerSelect;
            while(customerResultSet.next())
            {
                customerSelect = customerResultSet.getInt(1) + customerResultSet.getString(2) + customerResultSet.getString(3) + customerResultSet.getString(4) + customerResultSet.getString(5) + customerResultSet.getInt(6);
                if(customerInsertList.contains(customerSelect))
                    customerCounter++;
            }

            ResultSet productResultSet = statement.executeQuery("select * from product");
            String productSelect;
            while(productResultSet.next())
            {
                productSelect = productResultSet.getInt(1) + productResultSet.getString(2) + productResultSet.getString(3) + productResultSet.getInt(4) + productResultSet.getInt(5);
                if(productInsertList.contains(productSelect))
                    productCounter++;
            }

            ResultSet purchaseResultSet = statement.executeQuery("select * from purchase");
            String purchaseSelect;
            while(purchaseResultSet.next())
            {
                purchaseSelect = Integer.toString(purchaseResultSet.getInt(1)) + purchaseResultSet.getInt(2) + purchaseResultSet.getInt(3) + purchaseResultSet.getInt(4);
                if(purchaseInsertList.contains(purchaseSelect))
                    purchaseCounter++;
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Sprawdzana tabela: CUSTOMER. Poprawność rekordów: " + customerCounter + "/" + customerInsertList.size());
        System.out.println("Sprawdzana tabela: PRODUCT. Poprawność rekordów: " + productCounter + "/" + productInsertList.size());
        System.out.println("Sprawdzana tabela: PURCHASE. Poprawność rekordów: " + purchaseCounter + "/" + purchaseInsertList.size());
    }


    private static ArrayList<String> prepareInsertCheckList(String filePath, String tableName)
    {
        ArrayList<String> listInsert = new ArrayList<String>();
        FileReader fileReader;
        BufferedReader bufferedReader = null;
        try
        {
            fileReader = new FileReader(filePath);
            bufferedReader = new BufferedReader(fileReader);
            String textLine = bufferedReader.readLine();
            do
            {
                textLine = bufferedReader.readLine();
                if(textLine!=null)
                    if(textLine.startsWith("INSERT"))
                        if(textLine.indexOf(tableName)>0)
                            listInsert.add(textLine);
            }
            while (textLine != null);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        ArrayList<String> outListInsert = new ArrayList<String>();
        String data;
        Iterator<String> iterator = listInsert.iterator();
        while(iterator.hasNext())
        {
            data = iterator.next();
            data = data.substring(data.indexOf('(')+1, data.indexOf(')'));
            data = data.replaceAll("'", "");
            data = data.replaceAll(", ", "");
//            System.out.println(data);
            outListInsert.add(data);
        }

        return outListInsert;
    }
}
