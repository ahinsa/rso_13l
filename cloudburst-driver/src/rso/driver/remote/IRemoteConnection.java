/**
 * IRemoteConnection - Interfejs udostępnia zdalnie podstawowe metody
 * Connection.
 */
package rso.driver.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

public interface IRemoteConnection extends Remote {

    public IRemoteStatement createStatement() throws RemoteException;


    public PreparedStatement prepareStatement(String sql) throws RemoteException;


    public CallableStatement prepareCall(String sql) throws RemoteException;


    public String nativeSQL(String sql) throws RemoteException;


    public void setAutoCommit(boolean autoCommit) throws RemoteException;


    public boolean getAutoCommit() throws RemoteException;


    public void commit() throws RemoteException;


    public void rollback() throws RemoteException;


    public void close() throws RemoteException;


    public boolean isClosed() throws RemoteException;


    public DatabaseMetaData getMetaData() throws RemoteException;


    public void setReadOnly(boolean readOnly) throws RemoteException;


    public boolean isReadOnly() throws RemoteException;


    public void setCatalog(String catalog) throws RemoteException;


    public String getCatalog() throws RemoteException;


    public void setTransactionIsolation(int level) throws RemoteException;


    public int getTransactionIsolation() throws RemoteException;


    public SQLWarning getWarnings() throws RemoteException;


    public void clearWarnings() throws RemoteException;


    public IRemoteStatement createStatement(int resultSetType, int resultSetConcurrency) throws RemoteException;


    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws RemoteException;


    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws RemoteException;


    public Map<String, Class<?>> getTypeMap() throws RemoteException;


    public void setTypeMap(Map<String, Class<?>> map) throws RemoteException;


    public void setHoldability(int holdability) throws RemoteException;


    public int getHoldability() throws RemoteException;


    public Savepoint setSavepoint() throws RemoteException;


    public Savepoint setSavepoint(String name) throws RemoteException;


    public void rollback(Savepoint savepoint) throws RemoteException;


    public void releaseSavepoint(Savepoint savepoint) throws RemoteException;


    public IRemoteStatement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws RemoteException;


    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws RemoteException;


    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws RemoteException;


    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws RemoteException;


    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws RemoteException;


    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws RemoteException;


    public Clob createClob() throws RemoteException;


    public Blob createBlob() throws RemoteException;


    public NClob createNClob() throws RemoteException;


    public SQLXML createSQLXML() throws RemoteException;


    public boolean isValid(int timeout) throws RemoteException;


    public void setClientInfo(String name, String value) throws RemoteException;


    public void setClientInfo(Properties properties) throws RemoteException;


    public String getClientInfo(String name) throws RemoteException;


    public Properties getClientInfo() throws RemoteException;


    public Array createArrayOf(String typeName, Object[] elements) throws RemoteException;


    public Struct createStruct(String typeName, Object[] attributes) throws RemoteException;


    public void setSchema(String schema) throws RemoteException;


    public String getSchema() throws RemoteException;


    public void abort(Executor executor) throws RemoteException;


    public void setNetworkTimeout(Executor executor, int milliseconds) throws RemoteException;


    public int getNetworkTimeout() throws RemoteException;


    public <T> T unwrap(Class<T> iface) throws RemoteException;


    public boolean isWrapperFor(Class<?> iface) throws RemoteException;
}