/**
 * IRemoteDriver - Interfejs udostępnia zdalnie podstawowe metody Driver'a.
 */
package rso.driver.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;

public interface IRemoteDriver extends Remote {
    IRemoteConnection getConnection(String username, String password, String dbName, boolean b) throws RemoteException, SQLException;
    public void registerNode(String url) throws RemoteException;
}