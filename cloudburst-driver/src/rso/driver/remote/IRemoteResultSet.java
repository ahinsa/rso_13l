/**
 * IRemoteResultSet - Interfejs udostępnia zdalnie podstawowe metody ResultSet.
 */

package rso.driver.remote;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

public interface IRemoteResultSet extends Remote {
    public boolean next() throws RemoteException;

    public void close() throws RemoteException;

    public boolean wasNull() throws RemoteException;

    public String getString(int columnIndex) throws RemoteException;

    public boolean getBoolean(int columnIndex) throws RemoteException;

    public byte getByte(int columnIndex) throws RemoteException;

    public short getShort(int columnIndex) throws RemoteException;

    public int getInt(int columnIndex) throws RemoteException;

    public long getLong(int columnIndex) throws RemoteException;

    public float getFloat(int columnIndex) throws RemoteException;

    public double getDouble(int columnIndex) throws RemoteException;

    public BigDecimal getBigDecimal(int columnIndex, int scale) throws RemoteException;

    public byte[] getBytes(int columnIndex) throws RemoteException;

    public Date getDate(int columnIndex) throws RemoteException;

    public Time getTime(int columnIndex) throws RemoteException;

    public Timestamp getTimestamp(int columnIndex) throws RemoteException;

    public InputStream getAsciiStream(int columnIndex) throws RemoteException;

    public InputStream getUnicodeStream(int columnIndex) throws RemoteException;

    public InputStream getBinaryStream(int columnIndex) throws RemoteException;

    public String getString(String columnLabel) throws RemoteException;

    public boolean getBoolean(String columnLabel) throws RemoteException;

    public byte getByte(String columnLabel) throws RemoteException;

    public short getShort(String columnLabel) throws RemoteException;

    public int getInt(String columnLabel) throws RemoteException;

    public long getLong(String columnLabel) throws RemoteException;

    public float getFloat(String columnLabel) throws RemoteException;

    public double getDouble(String columnLabel) throws RemoteException;

    public BigDecimal getBigDecimal(String columnLabel, int scale) throws RemoteException;

    public byte[] getBytes(String columnLabel) throws RemoteException;

    public Date getDate(String columnLabel) throws RemoteException;

    public Time getTime(String columnLabel) throws RemoteException;

    public Timestamp getTimestamp(String columnLabel) throws RemoteException;

    public InputStream getAsciiStream(String columnLabel) throws RemoteException;

    public InputStream getUnicodeStream(String columnLabel) throws RemoteException;

    public InputStream getBinaryStream(String columnLabel) throws RemoteException;

    public SQLWarning getWarnings() throws RemoteException;

    public void clearWarnings() throws RemoteException;

    public String getCursorName() throws RemoteException;

    public ResultSetMetaData getMetaData() throws RemoteException;

    public Object getObject(int columnIndex) throws RemoteException;

    public Object getObject(String columnLabel) throws RemoteException;

    public int findColumn(String columnLabel) throws RemoteException;

    public Reader getCharacterStream(int columnIndex) throws RemoteException;

    public Reader getCharacterStream(String columnLabel) throws RemoteException;

    public BigDecimal getBigDecimal(int columnIndex) throws RemoteException;

    public BigDecimal getBigDecimal(String columnLabel) throws RemoteException;

    public boolean isBeforeFirst() throws RemoteException;

    public boolean isAfterLast() throws RemoteException;

    public boolean isFirst() throws RemoteException;

    public boolean isLast() throws RemoteException;

    public void beforeFirst() throws RemoteException;

    public void afterLast() throws RemoteException;

    public boolean first() throws RemoteException;

    public boolean last() throws RemoteException;

    public int getRow() throws RemoteException;

    public boolean absolute(int row) throws RemoteException;

    public boolean relative(int rows) throws RemoteException;

    public boolean previous() throws RemoteException;

    public void setFetchDirection(int direction) throws RemoteException;

    public int getFetchDirection() throws RemoteException;

    public void setFetchSize(int rows) throws RemoteException;

    public int getFetchSize() throws RemoteException;

    public int getType() throws RemoteException;

    public int getConcurrency() throws RemoteException;

    public boolean rowUpdated() throws RemoteException;

    public boolean rowInserted() throws RemoteException;

    public boolean rowDeleted() throws RemoteException;

    public void updateNull(int columnIndex) throws RemoteException;

    public void updateBoolean(int columnIndex, boolean x) throws RemoteException;

    public void updateByte(int columnIndex, byte x) throws RemoteException;

    public void updateShort(int columnIndex, short x) throws RemoteException;

    public void updateInt(int columnIndex, int x) throws RemoteException;

    public void updateLong(int columnIndex, long x) throws RemoteException;

    public void updateFloat(int columnIndex, float x) throws RemoteException;

    public void updateDouble(int columnIndex, double x) throws RemoteException;

    public void updateBigDecimal(int columnIndex, BigDecimal x) throws RemoteException;

    public void updateString(int columnIndex, String x) throws RemoteException;

    public void updateBytes(int columnIndex, byte[] x) throws RemoteException;

    public void updateDate(int columnIndex, Date x) throws RemoteException;

    public void updateTime(int columnIndex, Time x) throws RemoteException;

    public void updateTimestamp(int columnIndex, Timestamp x) throws RemoteException;

    public void updateAsciiStream(int columnIndex, InputStream x, int length) throws RemoteException;

    public void updateBinaryStream(int columnIndex, InputStream x, int length) throws RemoteException;

    public void updateCharacterStream(int columnIndex, Reader x, int length) throws RemoteException;

    public void updateObject(int columnIndex, Object x, int scaleOrLength) throws RemoteException;

    public void updateObject(int columnIndex, Object x) throws RemoteException;

    public void updateNull(String columnLabel) throws RemoteException;

    public void updateBoolean(String columnLabel, boolean x) throws RemoteException;

    public void updateByte(String columnLabel, byte x) throws RemoteException;

    public void updateShort(String columnLabel, short x) throws RemoteException;

    public void updateInt(String columnLabel, int x) throws RemoteException;

    public void updateLong(String columnLabel, long x) throws RemoteException;

    public void updateFloat(String columnLabel, float x) throws RemoteException;

    public void updateDouble(String columnLabel, double x) throws RemoteException;

    public void updateBigDecimal(String columnLabel, BigDecimal x) throws RemoteException;

    public void updateString(String columnLabel, String x) throws RemoteException;

    public void updateBytes(String columnLabel, byte[] x) throws RemoteException;

    public void updateDate(String columnLabel, Date x) throws RemoteException;

    public void updateTime(String columnLabel, Time x) throws RemoteException;

    public void updateTimestamp(String columnLabel, Timestamp x) throws RemoteException;

    public void updateAsciiStream(String columnLabel, InputStream x, int length) throws RemoteException;

    public void updateBinaryStream(String columnLabel, InputStream x, int length) throws RemoteException;

    public void updateCharacterStream(String columnLabel, Reader reader, int length) throws RemoteException;

    public void updateObject(String columnLabel, Object x, int scaleOrLength) throws RemoteException;

    public void updateObject(String columnLabel, Object x) throws RemoteException;

    public void insertRow() throws RemoteException;

    public void updateRow() throws RemoteException;

    public void deleteRow() throws RemoteException;

    public void refreshRow() throws RemoteException;

    public void cancelRowUpdates() throws RemoteException;

    public void moveToInsertRow() throws RemoteException;

    public void moveToCurrentRow() throws RemoteException;

    public Statement getStatement() throws RemoteException;

    public Object getObject(int columnIndex, Map<String, Class<?>> map) throws RemoteException;

    public Ref getRef(int columnIndex) throws RemoteException;

    public Blob getBlob(int columnIndex) throws RemoteException;

    public Clob getClob(int columnIndex) throws RemoteException;

    public Array getArray(int columnIndex) throws RemoteException;

    public Object getObject(String columnLabel, Map<String, Class<?>> map) throws RemoteException;

    public Ref getRef(String columnLabel) throws RemoteException;

    public Blob getBlob(String columnLabel) throws RemoteException;

    public Clob getClob(String columnLabel) throws RemoteException;

    public Array getArray(String columnLabel) throws RemoteException;

    public Date getDate(int columnIndex, Calendar cal) throws RemoteException;

    public Date getDate(String columnLabel, Calendar cal) throws RemoteException;

    public Time getTime(int columnIndex, Calendar cal) throws RemoteException;

    public Time getTime(String columnLabel, Calendar cal) throws RemoteException;

    public Timestamp getTimestamp(int columnIndex, Calendar cal) throws RemoteException;

    public Timestamp getTimestamp(String columnLabel, Calendar cal) throws RemoteException;

    public URL getURL(int columnIndex) throws RemoteException;

    public URL getURL(String columnLabel) throws RemoteException;

    public void updateRef(int columnIndex, Ref x) throws RemoteException;

    public void updateRef(String columnLabel, Ref x) throws RemoteException;

    public void updateBlob(int columnIndex, Blob x) throws RemoteException;

    public void updateBlob(String columnLabel, Blob x) throws RemoteException;

    public void updateClob(int columnIndex, Clob x) throws RemoteException;

    public void updateClob(String columnLabel, Clob x) throws RemoteException;

    public void updateArray(int columnIndex, Array x) throws RemoteException;

    public void updateArray(String columnLabel, Array x) throws RemoteException;

    public RowId getRowId(int columnIndex) throws RemoteException;

    public RowId getRowId(String columnLabel) throws RemoteException;

    public void updateRowId(int columnIndex, RowId x) throws RemoteException;

    public void updateRowId(String columnLabel, RowId x) throws RemoteException;

    public int getHoldability() throws RemoteException;

    public boolean isClosed() throws RemoteException;

    public void updateNString(int columnIndex, String nString) throws RemoteException;

    public void updateNString(String columnLabel, String nString) throws RemoteException;

    public void updateNClob(int columnIndex, NClob nClob) throws RemoteException;

    public void updateNClob(String columnLabel, NClob nClob) throws RemoteException;

    public NClob getNClob(int columnIndex) throws RemoteException;

    public NClob getNClob(String columnLabel) throws RemoteException;

    public SQLXML getSQLXML(int columnIndex) throws RemoteException;

    public SQLXML getSQLXML(String columnLabel) throws RemoteException;

    public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws RemoteException;

    public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws RemoteException;

    public String getNString(int columnIndex) throws RemoteException;

    public String getNString(String columnLabel) throws RemoteException;

    public Reader getNCharacterStream(int columnIndex) throws RemoteException;

    public Reader getNCharacterStream(String columnLabel) throws RemoteException;

    public void updateNCharacterStream(int columnIndex, Reader x, long length) throws RemoteException;

    public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws RemoteException;

    public void updateAsciiStream(int columnIndex, InputStream x, long length) throws RemoteException;

    public void updateBinaryStream(int columnIndex, InputStream x, long length) throws RemoteException;

    public void updateCharacterStream(int columnIndex, Reader x, long length) throws RemoteException;

    public void updateAsciiStream(String columnLabel, InputStream x, long length) throws RemoteException;

    public void updateBinaryStream(String columnLabel, InputStream x, long length) throws RemoteException;

    public void updateCharacterStream(String columnLabel, Reader reader, long length) throws RemoteException;

    public void updateBlob(int columnIndex, InputStream inputStream, long length) throws RemoteException;

    public void updateBlob(String columnLabel, InputStream inputStream, long length) throws RemoteException;

    public void updateClob(int columnIndex, Reader reader, long length) throws RemoteException;

    public void updateClob(String columnLabel, Reader reader, long length) throws RemoteException;

    public void updateNClob(int columnIndex, Reader reader, long length) throws RemoteException;

    public void updateNClob(String columnLabel, Reader reader, long length) throws RemoteException;

    public void updateNCharacterStream(int columnIndex, Reader x) throws RemoteException;

    public void updateNCharacterStream(String columnLabel, Reader reader) throws RemoteException;

    public void updateAsciiStream(int columnIndex, InputStream x) throws RemoteException;

    public void updateBinaryStream(int columnIndex, InputStream x) throws RemoteException;

    public void updateCharacterStream(int columnIndex, Reader x) throws RemoteException;

    public void updateAsciiStream(String columnLabel, InputStream x) throws RemoteException;

    public void updateBinaryStream(String columnLabel, InputStream x) throws RemoteException;

    public void updateCharacterStream(String columnLabel, Reader reader) throws RemoteException;

    public void updateBlob(int columnIndex, InputStream inputStream) throws RemoteException;

    public void updateBlob(String columnLabel, InputStream inputStream) throws RemoteException;

    public void updateClob(int columnIndex, Reader reader) throws RemoteException;

    public void updateClob(String columnLabel, Reader reader) throws RemoteException;

    public void updateNClob(int columnIndex, Reader reader) throws RemoteException;

    public void updateNClob(String columnLabel, Reader reader) throws RemoteException;

    public <T> T getObject(int columnIndex, Class<T> type) throws RemoteException;

    public <T> T getObject(String columnLabel, Class<T> type) throws RemoteException;

    public <T> T unwrap(Class<T> iface) throws RemoteException;

    public boolean isWrapperFor(Class<?> iface) throws RemoteException;

}