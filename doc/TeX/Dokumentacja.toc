\select@language {polish}
\contentsline {chapter}{\numberline {1}NewSQL}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Wst\k ep}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}R\'o\.znice }{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Pr\'oba scharakteryzowania NewSQL}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Architektura Shared Nothing}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Przyk\IeC {\l }ady rozwi\k aza\'n NewSQL}{4}{section.1.5}
\contentsline {section}{\numberline {1.6}Bibliografia}{5}{section.1.6}
\contentsline {chapter}{\numberline {2}Drizzle}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Architektura}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Replikacja}{7}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Protoko\IeC {\l }y sieciowe}{7}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Dziennik zdarze\'n}{7}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Metadane}{8}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Specjalne typy danych}{8}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Uwierzytelnianie, autoryzacja, dost\k ep do danych}{8}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Google Protocol Buffers}{8}{subsection.2.1.7}
\contentsline {section}{\numberline {2.2}API}{9}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Libdrizzle}{9}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Sterownik JDBC}{10}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Gearman Client}{10}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Memcached Client}{10}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}R\'o\.znice mi\k edzy Drizzle a MySQL}{10}{section.2.3}
\contentsline {chapter}{\numberline {3}Koncepcja rozwi\k azania}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Rozproszona baza danych}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Replikacja danych}{12}{section.3.2}
\contentsline {chapter}{\numberline {4}Szczeg\'o\IeC {\l }owa koncepcja rozwi\k azania}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Rozproszona baza danych}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}Architektura}{15}{section.4.2}
\contentsline {section}{\numberline {4.3}Ostateczne wykonanie}{16}{section.4.3}
\contentsline {chapter}{\numberline {5}Harmonogram realizacji projektu}{18}{chapter.5}
\contentsline {section}{\numberline {5.1}Komunikacja}{18}{section.5.1}
\contentsline {section}{\numberline {5.2}Podzia\IeC {\l } r\'ol w zespole}{18}{section.5.2}
\contentsline {section}{\numberline {5.3}Harmonogram}{19}{section.5.3}
\contentsline {section}{\numberline {5.4}Spotkania robocze}{20}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Pierwsza faza projektu}{20}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Druga faza projektu}{21}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Trzecia faza projektu}{21}{subsection.5.4.3}
\contentsline {section}{\numberline {5.5}Dziennik do\'swiadcze\'n}{22}{section.5.5}
\contentsline {chapter}{\numberline {6}\'Srodowisko programistyczne}{24}{chapter.6}
\contentsline {section}{\numberline {6.1}Wst\k ep}{24}{section.6.1}
\contentsline {section}{\numberline {6.2}Stworzenie maszyny wirtualnej}{24}{section.6.2}
\contentsline {section}{\numberline {6.3}Instalacja systemu}{24}{section.6.3}
\contentsline {section}{\numberline {6.4}Konfiguracja systemu}{29}{section.6.4}
\contentsline {section}{\numberline {6.5}Instalacja bazy danych Drizzle}{30}{section.6.5}
\contentsline {section}{\numberline {6.6}Uruchamianie test\'ow}{31}{section.6.6}
\contentsline {section}{\numberline {6.7}Uruchomienie gotowego obrazu maszyny wirtualnej}{31}{section.6.7}
\contentsline {section}{\numberline {6.8}Tworzenie sieci z kilku wirtualnych maszyn}{32}{section.6.8}
\contentsline {section}{\numberline {6.9}Wyb\'or j\k ezyka programowania}{32}{section.6.9}
\contentsline {subsection}{\numberline {6.9.1}Python}{32}{subsection.6.9.1}
\contentsline {subsection}{\numberline {6.9.2}C++}{33}{subsection.6.9.2}
\contentsline {subsection}{\numberline {6.9.3}C\#}{33}{subsection.6.9.3}
\contentsline {subsection}{\numberline {6.9.4}Java}{33}{subsection.6.9.4}
\contentsline {chapter}{\numberline {7}Koncepcja test\'ow}{35}{chapter.7}
\contentsline {section}{\numberline {7.1}Opracowanie koncepcji test\'ow}{35}{section.7.1}
\contentsline {section}{\numberline {7.2}Konfiguracja interfejs\'ow}{36}{section.7.2}
\contentsline {section}{\numberline {7.3}Konfiguracja master-slave}{36}{section.7.3}
\contentsline {section}{\numberline {7.4}Wykonanie test\'ow}{39}{section.7.4}
\contentsline {chapter}{\numberline {8}Prezentacja wynik\'ow}{40}{chapter.8}
