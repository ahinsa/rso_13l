--Tworzenie tabel

CREATE TABLE customer (
  id NUMBER,
  name VARCHAR2(20),
  surname VARCHAR2(32),
  city VARCHAR2(16),
  street VARCHAR2(16),
  nr NUMBER(3)
);

CREATE TABLE product (
  id NUMBER,
  name VARCHAR2(20),
  producer VARCHAR2(20),
  price NUMBER(5),
  weight NUMBER(5)
);

CREATE TABLE purchase (
  id_customer NUMBER,
  id_product NUMBER,
  quantity NUMBER(5)
);

ALTER TABLE customer ADD CONSTRAINT customer_id_pk PRIMARY KEY(id);
ALTER TABLE product ADD CONSTRAINT product_id_pk PRIMARY KEY(id);
ALTER TABLE purchase ADD CONSTRAINT purchase_id_pk PRIMARY KEY(id_customer, id_product);

