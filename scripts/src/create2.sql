--Tworzenie tabel

CREATE TABLE customer (id INT, name VARCHAR(20), surname VARCHAR(32), city VARCHAR(16), street VARCHAR(16), nr INT);

CREATE TABLE product (id INT, name VARCHAR(20), producer VARCHAR(20), price INT, weight INT);

CREATE TABLE purchase (id_customer INT, id_product INT, quantity INT);

ALTER TABLE customer ADD CONSTRAINT customer_id_pk PRIMARY KEY(id);
ALTER TABLE product ADD CONSTRAINT product_id_pk PRIMARY KEY(id);
ALTER TABLE purchase ADD CONSTRAINT purchase_id_pk PRIMARY KEY(id_customer, id_product);

