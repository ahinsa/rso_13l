#!/bin/bash

file_name="insert.sql"
number_of_customers=100
number_of_products=100
number_of_purchases=1000

#INSERT INTO  customer VALUES (1, 1234567890,35, 't');
#CREATE TABLE customer (
#  id NUMBER,
#  name VARCHAR2(20),
#  surname VARCHAR2(32),
#  city VARCHAR2(16),
#  street VARCHAR2(16),
#  nr NUMBER(3)
#);
echo "--TABLE customer (id NUMBER, name VARCHAR2(20), surname VARCHAR2(32), city VARCHAR2(16), street VARCHAR2(16), nr NUMBER(3))" > $file_name
for((i=1; i<=$number_of_customers; i++)); do
	nr=$((RANDOM%99+1))
	echo "INSERT INTO  customer VALUES ($i, 'Name$i', 'Surname$i', 'City"$((RANDOM%(number_of_customers/10)+1))"', 'Street"$((RANDOM%(number_of_customers/10)+1))"', $nr);" >> $file_name
done

#CREATE TABLE product (
#  id NUMBER,
#  name VARCHAR2(20),
#  producer VARCHAR2(20),
#  price NUMBER(5),
#  weight NUMBER(5)
#);

echo "" >> $file_name
echo "-- TABLE product (id NUMBER, name VARCHAR2(20), producer VARCHAR2(20), price NUMBER(5), weight NUMBER(5))" >> $file_name
for((i=1; i<=$number_of_products; i++)); do
	echo "INSERT INTO  product VALUES ($i, 'Product$i', 'Firm"$((RANDOM%(number_of_products/10)+1))"', "$((RANDOM%999+1))", "$((RANDOM%99+1))");" >> $file_name
done

#CREATE TABLE purchase (
#  id_customer NUMBER,
#  id_product NUMBER,
#  quantity NUMBER(5)
#);

echo "" >> $file_name
echo "-- TABLE purchase (id_customer NUMBER, id_product NUMBER, quantity NUMBER(5)" >> $file_name
for((i=1; i<=$number_of_purchases; i++)); do
	echo "INSERT INTO  purchase VALUES ($i, "$((RANDOM%number_of_customers+1))", "$((RANDOM%number_of_products+1))", "$((RANDOM%999+1))");" >> $file_name
done

